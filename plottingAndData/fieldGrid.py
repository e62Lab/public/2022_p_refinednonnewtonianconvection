# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
import json
import gzip
import matplotlib.tri as triang
import cmocean
from mpl_toolkits.axes_grid1 import make_axes_locatable

plt.rc('text', usetex = False)
#plt.rc('font', size = 14, family = 'serif', serif = ['Arial'])
plt.rc('font', size = 14, family = 'serif', serif = ['STIXGeneral'])
plt.rc('xtick', labelsize = 'small')
plt.rc('ytick', labelsize = 'small')
plt.rc('legend', frameon = True, fontsize = 'medium', loc = "best")
plt.rc('figure', figsize = (10, 6), facecolor = "white")
plt.rc('lines', linewidth=1.0)
plt.rc('axes', xmargin = 0, ymargin = 0, autolimit_mode = 'round_numbers')
plt.rc('mathtext', fontset = 'cm', rm = 'serif')

plt.close('all')

def toScientific(number, decimals = 1):
    try:
        if number == np.inf:
            return "$\infty$"
        exponent = int(np.floor(np.log10(np.abs(number))))
        if decimals < 0:
            return f"$10^{ {exponent} }$"
        elif exponent == 0:
            return str(np.round(number, decimals))
        elif exponent == 1:
            return str(np.round(number, decimals))
        elif exponent == 2:
            return str(np.round(number, decimals))
        elif exponent == -1:
            return str(np.round(number, decimals+1))
        elif exponent == -2:
            return str(np.round(number, decimals+2))
        else:
            return f"${np.round(number / np.power(10.0, exponent), decimals)} \cdot 10^{ {exponent} }$"
    except Exception as e:
        print("Got exception: ", e)
        return "NaN"

#------------------------------------------------------------------------------
with gzip.open("plotData/monOrderConvergence.json.gz", 'r') as zipfile:
    data = json.load(zipfile)

# %% Velocity scatter temperature contour plots

rayleighs = [10000, 100000, 1000000]
power_indices = [0.6, 0.8, 1]
mon_order = 4
averaging = 10
colorLevels = 30

figVel, axVel = plt.subplots(len(power_indices), len(rayleighs), figsize=(10, 10),
                              sharex=True, sharey=True)

colIdx = -1
for Ra in rayleighs:
    rowIdx = -1
    colIdx += 1
    for n in power_indices:
        rowIdx += 1
        for ax in [axVel]:
            ax[0, colIdx].set_title(f"Ra = {toScientific(Ra, -1)}\n")
        best_contents = None
        min_step = np.inf
        for filename, contents in data.items():
            attrs = contents["attrs"]
            if attrs["phy.power_index"] != n or attrs["dimensionless.Ra"] != Ra or attrs["num.mon_order"] != mon_order: continue
            if attrs["num.h"] < min_step:
                best_contents = contents
                min_step = attrs["num.h"]

        attrs = best_contents["attrs"]
        print(attrs["dimensionless.Ra"])
        pos = best_contents["pos"]
        v = np.array(best_contents["v"])
        T = np.array(best_contents["T"])
        v = v * attrs["phy.c_p"] * attrs["phy.rho"] * attrs["case.height"] / attrs["phy.lam"]
        absVel = np.sqrt(np.sum(v**2, axis=0))
        maxVel = np.max(absVel)
        
        levels = np.linspace(0, maxVel, colorLevels)
        
        tri = triang.Triangulation(*pos)
        # imgVel = ax[rowIdx, colIdx].tricontourf(tri, absVel, cmap = cmocean.cm.amp, levels=levels)
        imgVel = axVel[rowIdx, colIdx].scatter(*pos, s=0.2, c=absVel, cmap=cmocean.cm.amp, vmin=0, vmax=maxVel)
        imgT = axVel[rowIdx, colIdx].tricontour(tri, T, colors='k', alpha = 0.7, linestyles="solid")
        axVel[rowIdx, colIdx].set_aspect("equal")
    
        pad = 0.05
        divider = make_axes_locatable(axVel[rowIdx, colIdx])
        cax = divider.append_axes("top", size="5%", pad=pad)
        plt.colorbar(imgVel, cax=cax, ticks=[0, maxVel], orientation="horizontal")#, label="Velocity magnitude")
        cax.xaxis.set_ticks_position('top')
        
        for ax in [axVel]:
            if colIdx == 0:
                ax[rowIdx, 0].set_ylabel( f"$n = {n}$" + "\n" + r"$y$")
            if rowIdx == len(ax) - 1:
                ax[rowIdx, colIdx].set_xlabel("$x$")

figVel.tight_layout()

figVel.savefig("../figures/velScatterTContour", dpi=300)