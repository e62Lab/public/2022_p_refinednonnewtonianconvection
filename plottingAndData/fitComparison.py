# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
import h5py
import os
import gzip
import json
from texttable import Texttable
import latextable
import cmocean

import nusseltFits

plt.rc('text', usetex = False)
#plt.rc('font', size = 14, family = 'serif', serif = ['Arial'])
plt.rc('font', size = 14, family = 'serif', serif = ['STIXGeneral'])
plt.rc('xtick', labelsize = 'small')
plt.rc('ytick', labelsize = 'small')
plt.rc('legend', frameon = True, fontsize = 'medium', loc = "best")
plt.rc('figure', figsize = (10, 6), facecolor = "white")
plt.rc('lines', linewidth=1.0)
plt.rc('axes', xmargin = 0, ymargin = 0, autolimit_mode = 'round_numbers')
plt.rc('mathtext', fontset = 'cm', rm = 'serif')

plt.close('all')

def toScientific(number, decimals = 1):
    try:
        if number == np.inf:
            return "$\infty$"
        exponent = int(np.floor(np.log10(np.abs(number))))
        if decimals < 0:
            return f"$10^{ {exponent} }$"
        elif exponent == 0:
            return str(np.round(number, decimals))
        elif exponent == 1:
            return str(np.round(number, decimals))
        elif exponent == 2:
            return str(np.round(number, decimals))
        elif exponent == -1:
            return str(np.round(number, decimals+1))
        elif exponent == -2:
            return str(np.round(number, decimals+2))
        else:
            return f"${np.round(number / np.power(10.0, exponent), decimals)} \cdot 10^{ {exponent} }$"
    except Exception as e:
        print("Got exception: ", e)
        return "NaN"

# %%------------------------------------------------------------------------------
jsonPath = "plotData/refinedConvergence.json.gz"
with gzip.open(jsonPath, 'r') as zipfile:
    refinedData = json.load(zipfile)
with gzip.open("plotData/monOrderConvergence.json.gz", 'r') as zipfile:
    convergenceData = json.load(zipfile)

# %% Comparison with known Nu(Ra, Pr, n) relations
tableMon = 4
tablePrecision = 2
rayleighs = [10000, 100000, 1000000]
Pr = 100
monOrders = [2, 4]
powerIndices = [0.6, 0.7, 0.8, 0.9, 1]

table = Texttable()
table.set_precision(tablePrecision)
table.header(["Ra"] + [f"$n = {n}$" for n in powerIndices])

fig, ax = plt.subplots(1, len(rayleighs), sharex=True, figsize=(10, 4))
rowIdx = 0
for Ra in rayleighs:
    n = np.linspace(0.6, 1, 1000)
    ax[rowIdx].plot(n, nusseltFits.turan(n, Ra, Pr), label = "Turan et al. fit")
    ax[rowIdx].plot(n, nusseltFits.kim(n, Ra, Pr), label = "Kim et al. fit")
    markers = iter(["x", "+"])    
    for mon in monOrders:
        marker = next(markers)
        plotx, ploty = [], []
        for n in powerIndices:
            # Find the smallest spatial step that is completed for all support sizes
            # Look for refined solutions first
            refinedExists = False
            minStep = float("inf")
            plotx.append(-1)
            ploty.append(-1)
            for name, contents in refinedData.items():
                params = contents["attrs"]
                # Identify that the refined solution for given case exists
                if (params["dimensionless.Ra"] == Ra and params["dimensionless.Pr"] == Pr and params["phy.power_index"] == n):
                    refinedExists = True
                    # Add to data if support size matches
                    if (params["num.mon_order"] == mon and params["num.h"] < minStep):
                        minStep = params["num.h"]
                        plotx[-1] = n
                        ploty[-1] = contents["Nu"][-1]
            if not refinedExists: # Search among dense
                minStep = float("inf")
                for name, contents in convergenceData.items():
                    params = contents["attrs"]
                    # Identify that the refined solution for given case exists
                    if (params["dimensionless.Ra"] == Ra and params["dimensionless.Pr"] == Pr and params["phy.power_index"] == n):
                        refinedExists = True
                        # Add to data if support size matches
                        if (params["num.mon_order"] == mon and params["num.h"] < minStep):
                            minStep = params["num.h"]
                            plotx[-1] = n
                            ploty[-1] = contents["Nu"][-1]
        if len(plotx) > 0:  
            ax[rowIdx].plot(plotx, ploty, marker, label = f"$m={mon}$", clip_on=False)
        ax[rowIdx].set_title(f"Ra$=${toScientific(Ra, -1)}")
        if mon == tableMon:
            table.add_row([f"{toScientific(Ra, -1)}"] + ploty)
    rowIdx += 1
print(table.draw())
print(latextable.draw_latex(table))
# ax[-1].plot(0.6, 33.6196, 'k.', label = "Turan convergence study")

handles, labels = ax[-1].get_legend_handles_labels()
ax[2].legend(handles[:2], labels[:2])
ax[0].legend(handles[2:4], labels[2:4])
# ax[-1].set_xlabel("$n$")
ax[0].set_ylabel(r"$\overline{\mathrm{Nu}}$")
for a in ax:
    # a.set_ylabel(r"$\overline{\mathrm{Nu}}$")
    a.set_xlabel("$n$")
    a.grid(linestyle="-")

fig.tight_layout()

# fig.savefig("../figures/fitComparison.pdf")
# fig.savefig("../figures/fitComparison", dpi=300)