# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
import json
import gzip
import scipy.interpolate as interpolate
import cmocean

import nusseltFits

plt.rc('text', usetex = False)
#plt.rc('font', size = 14, family = 'serif', serif = ['Arial'])
plt.rc('font', size = 14, family = 'serif', serif = ['STIXGeneral'])
plt.rc('xtick', labelsize = 'small')
plt.rc('ytick', labelsize = 'small')
plt.rc('legend', frameon = True, fontsize = 'medium', loc = "best")
plt.rc('figure', figsize = (10, 6), facecolor = "white")
plt.rc('lines', linewidth=1.0)
plt.rc('axes', xmargin = 0, ymargin = 0, autolimit_mode = 'round_numbers')
plt.rc('mathtext', fontset = 'cm', rm = 'serif')

plt.close('all')
#------------------------------------------------------------------------------
jsonPath = "plotData/refinedConvergence.json.gz"
with gzip.open(jsonPath, 'r') as zipfile:
    data = json.load(zipfile)

# %%

Ra = 1000000
Pr = 100
n = 0.6
startIdx = 1
cross_location = 0.75
coordinates = [cross_location, 1 - cross_location]
interp_count = 5000

fig, ax = plt.subplots(1, 2, figsize=(10, 4))
plot_data = {}
maxX = 0
for filename, content in sorted(data.items()):
    attrs = content["attrs"]
    if attrs["phy.power_index"] != n or attrs["dimensionless.Ra"] != Ra: continue
            
    nusselts = content["Nu"]
    
    N = content["N"]
    print(f"h1: {attrs['num.h']},   edge: {int(1 / attrs['num.h'])},   N: {N}")
    d = plot_data.setdefault(attrs["num.mon_order"], [[], [], []])
    d[0].append(N)
    d[1].append(np.average(nusselts[-1])) # Average to minimize oscillatory effects.
    
    pos = np.array(content["pos"])
    vy = np.array(content["v"][1])
    h = attrs["num.h"]
    
    interpolated = []
    for idx in range(len(coordinates)):
        interp_loc = np.array([np.linspace(0, 1, interp_count), np.ones(interp_count) * coordinates[idx]]).T
        relevant_indices = ((pos[1] > coordinates[idx] - 5 * h) * (pos[1] < coordinates[idx] + 5 * h))
        interpolated.append(interpolate.griddata(pos[:, relevant_indices].T, vy[relevant_indices], interp_loc,
                            method="cubic"))
        if idx != 0: # Invert the second velocity profile for direct comparison
            interpolated[idx] *= -1
            interpolated[idx] = interpolated[idx][::-1]
    
    d[2].append(max(np.abs(interpolated[1] - interpolated[0])) / np.max(np.abs([interpolated[1], interpolated[0]])))

for key, values in sorted(plot_data.items(), key=lambda x: x[0]):
    order = np.argsort(values[0])
    for idx in range(len(values)):
        values[idx] = [values[idx][i] for i in order]
    ax[0].plot(values[0], values[1], '.--', label=f"$m={key:.0f}$")
    ax[1].plot(values[0], values[2], '.--', label=f"$m={key:.0f}$")
    maxX = max(maxX, max(values[0]))

ax[1].set_yscale("log")
ax[0].set_ylabel(r"$\overline{\mathrm{Nu}}$")
# ax[1].set_ylabel( f"$n = {n}$" + "\n" + r"$v_y$ relative symmetry error")
ax[1].set_ylabel(r"$\epsilon$")
for a in ax:
    a.set_xlabel("$N$")
    a.grid(linestyle="-")
ax[0].axhline(nusseltFits.turan(n, Ra, Pr), c="r", label="Turan et al. fit", alpha=0.95)
ax[0].axhline(nusseltFits.kim(n, Ra, Pr), c="b", label="Kim et al. fit", alpha=0.95)
if Ra == 1e6 and Pr == 100 and n == 0.6:
    ax[0].axhline(33.63, c="k", label="Turan et al. convergence", alpha=0.95)

handles, labels = ax[0].get_legend_handles_labels()
ax[1].legend(handles[:2], labels[:2])
ax[0].legend(handles[2:], labels[2:])

plt.tight_layout()

# fig.savefig(f"../figures/refinedConvergence{str(n).replace('.', '')}.pdf")
# fig.savefig(f"../figures/refinedConvergence{str(n).replace('.', '')}", dpi=300)


# %% log-log error

plot_data = {}
for filename, content in sorted(data.items()):
    attrs = content["attrs"]
    if attrs["phy.power_index"] != n or attrs["dimensionless.Ra"] != Ra: continue
            
    nusselts = content["Nu"]
    
    N = content["N"]
    d = plot_data.setdefault(attrs["num.mon_order"], [[], [], []])
    d[0].append(attrs["num.h"])
    d[1].append(np.average(nusselts[-1])) # Average to minimize oscillatory effects.



fig, ax = plt.subplots(figsize=(6, 6))
for key, values in sorted(plot_data.items(), key=lambda x: x[0]):
    order = np.argsort(1 / np.array(values[0]))
    values[0] = [values[0][i] for i in order[:-1]]
    values[1] = [abs(values[1][i] - values[1][order[-1]]) for i in order[:-1]]
            
    ax.plot(values[0], values[1], '.--', label=f"$m={key:.0f}$")
    maxX = max(maxX, max(values[0]))

minx = 1.2e-2
maxx = 7e-4
lsp = np.linspace(minx, maxx, 3)
ax.plot(lsp, np.power(100 * lsp, 2), 'k:', alpha=0.8, label="$\mathcal{O}(h^2)$")
ax.plot(lsp, np.power(1000 * lsp, 4), 'k-.', alpha=0.8, label="$\mathcal{O}(h^4)$")
ax.set_xlim(minx, maxx)
ax.set_yscale("log")
ax.set_xscale("log")
ax.xaxis.set_inverted(True)
ax.set_xlabel("$h$")
ax.set_ylabel(r"$| \overline{\mathrm{Nu}} - \overline{\mathrm{Nu}}_{h_{\mathrm{min}}}|$")
ax.legend()
plt.tight_layout()

# %% Load unrefined data
with gzip.open("plotData/monOrderConvergence.json.gz", 'r') as zipfile:
    unrefinedData = json.load(zipfile)

# %% Plot unified convergence

Ra = 1000000
Pr = 100
n = 0.7

fig, ax = plt.subplots(figsize=(8, 6))

refined = {}
maxX = 0
for filename, content in sorted(data.items()):
    attrs = content["attrs"]
    if attrs["phy.power_index"] != n or attrs["dimensionless.Ra"] != Ra: continue
            
    nusselts = content["Nu"]
    
    N = content["N"]
    # print(f"h1: {attrs['num.h']},   edge: {int(1 / attrs['num.h'])},   N: {N}")
    d = refined.setdefault(attrs["num.mon_order"], [[], []])
    d[0].append(N)
    d[1].append(nusselts[-1])
    

for key, values in sorted(refined.items(), key=lambda x: x[0]):
    order = np.argsort(values[0])
    for idx in range(len(values)):
        values[idx] = [values[idx][i] for i in order]
    ax.plot(values[0], values[1], '.--', label=f"refined $m={key:.0f}$")
    maxX = max(maxX, max(values[0]))

unrefined = {}
for filename, content in sorted(unrefinedData.items()):
    attrs = content["attrs"]
    if attrs["phy.power_index"] != n or attrs["dimensionless.Ra"] != Ra: continue
            
    nusselts = content["Nu"]
    
    N = content["N"]
    d = unrefined.setdefault(attrs["num.mon_order"], [[], []])
    d[0].append(N)
    d[1].append(nusselts[-1])
    

for key, values in sorted(unrefined.items(), key=lambda x: x[0]):
    order = np.argsort(values[0])
    for idx in range(len(values)):
        values[idx] = [values[idx][i] for i in order]
    ax.plot(values[0], values[1], '.--', label=f"unrefined $m={key:.0f}$")
    maxX = max(maxX, max(values[0]))

ax.set_ylabel(r"$\overline{\mathrm{Nu}}$")
ax.set_xlabel("$N$")
ax.grid(linestyle="-")
ax.axhline(nusseltFits.turan(n, Ra, Pr), c="r", label="Turan et al. fit", alpha=0.95)
ax.axhline(nusseltFits.kim(n, Ra, Pr), c="b", label="Kim et al. fit", alpha=0.95)
if Ra == 1e6 and Pr == 100 and n == 0.6:
    ax.axhline(33.63, c="k", label="Turan et al. convergence", alpha=0.95)
if n == 0.6:
    ax.set_ylim(30, 50)
if n == 0.7:
    ax.set_ylim(21, 28)
ax.set_xlim(0, 2.5e5)
ax.legend(ncol=2)
plt.tight_layout()

# fig.savefig(f"../figures/refinedUnrefinedConvergence{str(n).replace('.', '')}.pdf")
# fig.savefig(f"../figures/refinedUnrefinedConvergence{str(n).replace('.', '')}", dpi=300)