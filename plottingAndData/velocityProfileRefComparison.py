# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
import json
import gzip
import scipy.interpolate as interpolate
import matplotlib.ticker as mticker
import cmocean

plt.rc('text', usetex = False)
#plt.rc('font', size = 14, family = 'serif', serif = ['Arial'])
plt.rc('font', size = 14, family = 'serif', serif = ['STIXGeneral'])
plt.rc('xtick', labelsize = 'small')
plt.rc('ytick', labelsize = 'small')
plt.rc('legend', frameon = True, fontsize = 'medium', loc = "best")
plt.rc('figure', figsize = (10, 6), facecolor = "white")
plt.rc('lines', linewidth=1.0)
plt.rc('axes', xmargin = 0, ymargin = 0, zmargin=0, autolimit_mode = 'round_numbers')
plt.rc('mathtext', fontset = 'cm', rm = 'serif')

plt.close('all')

def toScientific(number, decimals = 1):
    try:
        if number == np.inf:
            return "$\infty$"
        exponent = int(np.floor(np.log10(np.abs(number))))
        if decimals < 0:
            return f"$10^{ {exponent} }$"
        elif exponent == 0:
            
            return str(np.round(number, decimals))
        elif exponent == 1:
            return str(np.round(number, decimals))
        elif exponent == 2:
            return str(np.round(number, decimals))
        elif exponent == -1:
            return str(np.round(number, decimals+1))
        elif exponent == -2:
            return str(np.round(number, decimals+2))
        else:
            return f"${np.round(number / np.power(10.0, exponent), decimals)} \cdot 10^{ {exponent} }$"
    except Exception as e:
        print("Got exception: ", e)
        return "NaN"

#------------------------------------------------------------------------------
with gzip.open("plotData/monOrderConvergence.json.gz", 'r') as zipfile:
    data = json.load(zipfile)

# %% Plot vertical velocity profiles and reference values

turan = {10000 : {"x":[0.881,0.917,0.949], "y":[20.139,39.12,97.685], "errx":0.025, "erry":[5, 5, 5]},
100000 : {"x":[0.922,0.95, 0.977], "y":[74.152,165.848,521.804], "errx":0.025, "erry":[10, 10, 20]},
1000000 : {"x":[0.957,0.975, 0.985], "y":[239.631,634.101, 2472.481], "errx":0.025, "erry":[50, 50, 100]}}

Pr = 100
monOrder = 4

min_time = 9.9
cross_location = 0.5
interp_count = 2000
includeBoundary = True
includeNodesCloserThan = 2/3
xlim = [0.8, 1]

fig, ax = plt.subplots(1, 3, figsize=(10, 4))

col = 0
for Ra in [int(1e4), int(1e5), int(1e6)]:
    colors = iter(plt.rcParams['axes.prop_cycle'].by_key()['color'])
    i = 0
    for n in [0.6, 0.8, 1]:
        c = next(colors)
        candidates = {}
        for name, contents in data.items():
            attrs = contents["attrs"]
            if (attrs["dimensionless.Ra"] == Ra and attrs["dimensionless.Pr"] == Pr
                    and attrs["num.mon_order"] == monOrder and attrs["phy.power_index"] == n):
                candidates[attrs["num.h"]] = contents
        
        discretizations = sorted(candidates.keys(), reverse=True)
        
        h = discretizations[-1]
        contents = candidates[discretizations[-1]]
        attrs = contents["attrs"]    
        if contents["t"][-1] < min_time: continue
        pos = np.array(contents["pos"])
        N = contents["N"]
        vy = np.array(contents["v"][1])
        vy = vy * attrs["phy.c_p"] * attrs["phy.rho"] * attrs["case.height"] / attrs["phy.lam"]
        
        if includeBoundary:
            closest_indices = np.nonzero((pos[1] >= cross_location - includeNodesCloserThan * h) * (pos[1] <= cross_location + includeNodesCloserThan * h)
                               * (pos[0] >= xlim[0]) * (pos[0] <= xlim[1]))[0]
        else:
            closest_indices = np.nonzero((pos[1] >= cross_location - includeNodesCloserThan * h) * (pos[1] <= cross_location + includeNodesCloserThan * h)
                               * (pos[0] >= xlim[0]) * (pos[0] <= xlim[1]) * (pos[0] >= 1e-10) * (pos[0] <= 1 - 1e-10))[0]
        closest_indices = closest_indices[np.argsort(pos[0, closest_indices])]
        # spl = interpolate.UnivariateSpline(pos[0, closest_indices], vy[closest_indices], s=100)
        # interp_loc = np.linspace(*xlim, interp_count)
        # interp_val = spl(interp_loc)
        # ax[col].plot(interp_loc, interp_val, color=c, label=f"$N={N}$")
        ax[col].plot(pos[0, closest_indices], vy[closest_indices], ".", color=c, label=f"$n={n}$", markersize=4)
        ax[col].set_title(f"Ra$=10^{{{np.log10(Ra):g}}}$")
    ax[col].errorbar(turan[Ra]["x"], turan[Ra]["y"], turan[Ra]["erry"], turan[Ra]["errx"], color="k", linestyle="None", capsize=5, elinewidth=2, label="Turan et al.")
    col += 1

for a in ax:
    a.set_xlim(0.85, 1)
    a.set_ylim(0)
    a.set_xlabel("$x$")
    a.grid(linestyle="-")
ax[0].set_ylabel("$v_y$")
ax[-1].legend()

fig.tight_layout()

fig.savefig("../figures/velocityProfileRefComparison.pdf")
fig.savefig("../figures/velocityProfileRefComparison", dpi=300)