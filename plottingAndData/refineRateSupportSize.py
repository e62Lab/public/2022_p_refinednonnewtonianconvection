# -*- coding: utf-8 -*-
import math
import matplotlib.pyplot as plt
import numpy as np
import json
import gzip
import scipy.interpolate as interpolate
import matplotlib.ticker as mticker
import cmocean

plt.rc('text', usetex = False)
#plt.rc('font', size = 14, family = 'serif', serif = ['Arial'])
plt.rc('font', size = 14, family = 'serif', serif = ['STIXGeneral'])
plt.rc('xtick', labelsize = 'small')
plt.rc('ytick', labelsize = 'small')
plt.rc('legend', frameon = True, fontsize = 'medium', loc = "best")
plt.rc('figure', figsize = (10, 6), facecolor = "white")
plt.rc('lines', linewidth=1.0)
plt.rc('axes', xmargin = 0, ymargin = 0, zmargin=0, autolimit_mode = 'round_numbers')
plt.rc('mathtext', fontset = 'cm', rm = 'serif')

plt.close('all')

def toScientific(number, decimals = 1):
    try:
        if number == np.inf:
            return "$\infty$"
        exponent = int(np.floor(np.log10(np.abs(number))))
        if decimals < 0:
            return f"$10^{ {exponent} }$"
        elif exponent == 0:
            
            return str(np.round(number, decimals))
        elif exponent == 1:
            return str(np.round(number, decimals))
        elif exponent == 2:
            return str(np.round(number, decimals))
        elif exponent == -1:
            return str(np.round(number, decimals+1))
        elif exponent == -2:
            return str(np.round(number, decimals+2))
        else:
            return f"${np.round(number / np.power(10.0, exponent), decimals)} \cdot 10^{ {exponent} }$"
    except Exception as e:
        print("Got exception: ", e)
        return "NaN"

#------------------------------------------------------------------------------
with gzip.open("plotData/refineRateSupportSize.json.gz", 'r') as zipfile:
    data = json.load(zipfile)


# %% Refinement ratios
for case, file_index in data.items():
    supportSizes = []
    for name, contents in file_index.items():
        if contents["support_size_factor"] not in supportSizes:
            supportSizes.append(contents["support_size_factor"])
    supportSizes = sorted(supportSizes)
    
    colors = iter(plt.rcParams['axes.prop_cycle'].by_key()['color'])
    fig, ax = plt.subplots(1, 2, figsize=(10, 3.5), gridspec_kw={'width_ratios': [2, 1]})
    for ssf in supportSizes:
        c = next(colors)
        x, y, size = [], [], []
        for name, contents in file_index.items():
            if contents["support_size_factor"] == ssf:
                nMon = math.comb(int(contents["mon_order"]) + 2, int(contents["mon_order"])) #WARNING: Hardcoded dim 2
                x.append(contents["refine_ratio"])
                y.append(contents["nusselt"])
                if contents["refine_ratio"] == 25:
                    ax[1].plot(contents["crossPosX"], contents["crossVy"], '.', color=c, label=f"$s={round(nMon * ssf) + 1}$")
                if ssf == 3:
                    size.append(contents["N"])
        order = np.argsort(x)
        ax[0].plot([x[i] for i in order], [y[i] for i in order], '.--', color=c, label=f"$s={round(nMon * ssf) + 1}$")
        if ssf == 3:
            print(case)
            print([f"{x[i]} -> {size[i]}" for i in order])
    ax[0].legend(ncol=len(supportSizes)//2)
    # ax[0].set_yscale("log")
    ax[0].set_xlabel(r"$\frac{h_2}{h_1}$")
    ax[0].set_ylabel(r"$\overline{\mathrm{Nu}}$")
    ax[0].grid(linestyle="-")
    ax[1].set_xlim(0.9, 1)
    ax[1].set_ylim(0)
    ax[1].set_xlabel("$x$")
    ax[1].set_ylabel("$v_y$")
    ax[1].grid(linestyle="-")
    
    fig.tight_layout()
    # fig.savefig(f"../figures/refineRateSupportSize_{case}.pdf")
    # fig.savefig(f"../figures/refineRateSupportSize_{case}", dpi=300)