# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import matplotlib.tri as triang
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np
import json
import gzip
import cmocean

plt.rc('text', usetex = False)
#plt.rc('font', size = 14, family = 'serif', serif = ['Arial'])
plt.rc('font', size = 14, family = 'serif', serif = ['STIXGeneral'])
plt.rc('xtick', labelsize = 'small')
plt.rc('ytick', labelsize = 'small')
plt.rc('legend', frameon = True, fontsize = 'medium', loc = "best")
plt.rc('figure', figsize = (10, 6), facecolor = "white")
plt.rc('lines', linewidth=1.0)
plt.rc('axes', xmargin = 0, ymargin = 0, autolimit_mode = 'round_numbers')
plt.rc('mathtext', fontset = 'cm', rm = 'serif')

plt.close('all')

def toScientific(number, decimals = 1):
    try:
        if number == np.inf:
            return "$\infty$"
        exponent = int(np.floor(np.log10(np.abs(number))))
        if decimals < 0:
            return f"$10^{ {exponent} }$"
        elif exponent == 0:
            return str(np.round(number, decimals))
        elif exponent == 1:
            return str(np.round(number, decimals))
        elif exponent == 2:
            return str(np.round(number, decimals))
        elif exponent == -1:
            return str(np.round(number, decimals+1))
        elif exponent == -2:
            return str(np.round(number, decimals+2))
        else:
            return f"${np.round(number / np.power(10.0, exponent), decimals)} \cdot 10^{ {exponent} }$"
    except Exception as e:
        print("Got exception: ", e)
        return "NaN"

with gzip.open("plotData/porousCases.json.gz", 'r') as zipfile:
    data = json.load(zipfile)

# %% porous2D - Side by side velocity and temperature for different n
colorLevels = 30
filenames = ["2D_porous_central06_n06.h5", "2D_porous_central06_n1.h5"]

fig, ax = plt.subplots(1, len(filenames), sharex=True, sharey=True)

colIdx = 0
for filename in filenames:
    d = data[filename]
    pos = np.array(d["pos"])
    v = np.array(d["v"])
    T = d["T"]
    v = v * d["attrs"]["phy.c_p"] * d["attrs"]["phy.rho"] * d["attrs"]["case.height"] / d["attrs"]["phy.lam"]
    absVel = np.sqrt(np.sum(v**2, axis=0))
    maxVel = np.max(absVel)
    
    levels = np.linspace(0, maxVel, colorLevels)
    
    tri = triang.Triangulation(*pos)
    t = tri.triangles
    mask = np.any(((pos[0, t] - pos[0, np.roll(t, 1, axis=1)])**2
                   + (pos[1, t] - pos[1, np.roll(t, 1, axis=1)])**2) > (3*d["attrs"]["num.h"])**2, axis=1)
    tri.set_mask(mask)
    
    imgVel = ax[colIdx].tricontourf(tri, absVel, cmap = cmocean.cm.amp, levels=levels)
    # imgVel = ax[colIdx].scatter(*pos, s=4, c=absVel, cmap=cmocean.cm.amp, vmin=0, vmax=maxVel)
    imgT = ax[colIdx].tricontour(tri, T, colors='k', alpha = 0.7, linestyles="solid")
    ax[colIdx].set_aspect("equal")

    pad = 0.05
    divider = make_axes_locatable(ax[colIdx])
    cax = divider.append_axes("top", size="5%", pad=pad)
    plt.colorbar(imgVel, cax=cax, ticks=[0, maxVel], orientation="horizontal")#, label="Velocity magnitude")
    cax.xaxis.set_ticks_position('top')
    
    ax[colIdx].set_xlabel("$x$")
    ax[colIdx].set_title(f"$n={d['attrs']['phy.power_index']}$\n\n")
    colIdx += 1

ax[0].set_ylabel("$y$")

fig.tight_layout()

# fig.savefig("../figures/porous2D", dpi=300)

# %% porous2Dviscosity - Viscosity plot
filename = "2D_porous_central06_n06.h5"
log = False
colorLevels = 30

pos = np.array(data[filename]["pos"])
viscosity = np.array(data[filename]["viscosity"])
if log:
    inverse = np.log(data[filename]["attrs"]["phy.mu"] / viscosity)
else:
    inverse = data[filename]["attrs"]["phy.mu"] / viscosity
fig, ax = plt.subplots(figsize=(7.5, 6))

tri = triang.Triangulation(*pos)
t = tri.triangles
mask = np.any(((pos[0, t] - pos[0, np.roll(t, 1, axis=1)])**2
               + (pos[1, t] - pos[1, np.roll(t, 1, axis=1)])**2) > (3*data[filename]["attrs"]["num.h"])**2, axis=1)
tri.set_mask(mask)
img = ax.tricontourf(tri, inverse, cmap=cmocean.cm.amp, levels=colorLevels)

# img = ax.scatter(pos[0], pos[1], 5, c=inverse, cmap=cmocean.cm.amp)

if log:
    plt.colorbar(img, label=r"$\ln(\frac{\mu_0}{\mu})$")
else:
    plt.colorbar(img, label=r"$\frac{\mu_0}{\mu}$")
ax.set_aspect("equal")
ax.set_xlabel("$x$")
ax.set_ylabel("$y$")

# fig.savefig("../figures/porous2Dviscosity", dpi=300)

# TODO: consider various display options: inverse or not and log vs normal colormap

# %% Get nusselts for unobstructed (commented because the data takes a while to load)

# with gzip.open("plotData/monOrderConvergence.json.gz", 'r') as zipfile:
#     monData = json.load(zipfile)
    
# Ra = 1e6
# powerIndices = [0.6, 1]
# h = 0.008
# m = 2

# print(f"Obstructed DVD")
# for filename, d in data.items():
#     print(f"n = {d['attrs']['phy.power_index']} -> Nu = {d['Nu'][-1]}   (h={d['attrs']['num.h']}, m={d['attrs']['num.mon_order']})")

# print(f"Unobstructed DVD @ Ra = {Ra} with m={m}")
# for filename, contents in monData.items():
#     attrs = contents["attrs"]
#     for n in powerIndices:
#         if (attrs["phy.power_index"] == n
#                 and attrs["dimensionless.Ra"] == Ra
#                 and attrs["num.h"] == h
#                 and attrs["num.mon_order"] == m):
#             nusselts = contents["Nu"]
#             print(f"n = {n} -> Nu = {nusselts[-1]}   (h={d['attrs']['num.h']})")


