# -*- coding: utf-8 -*-

from matplotlib.animation import FuncAnimation
import matplotlib.pyplot as plt
import numpy as np
import h5py
import os
import itertools
import matplotlib.tri as tri
import matplotlib.collections as collections
import cmocean

plt.rc('text', usetex = False)
#plt.rc('font', size = 14, family = 'serif', serif = ['Arial'])
plt.rc('font', size = 14, family = 'serif', serif = ['STIXGeneral'])
plt.rc('xtick', labelsize = 'small')
plt.rc('ytick', labelsize = 'small')
plt.rc('legend', frameon = True, fontsize = 'medium', loc = "best")
plt.rc('figure', figsize = (10, 6), facecolor = "white")
plt.rc('lines', linewidth=1.0)
plt.rc('axes', xmargin = 0, ymargin = 0, autolimit_mode = 'round_numbers')
plt.rc('mathtext', fontset = 'cm', rm = 'serif')

plt.close('all')

#------------------------------------------------------------------------------

dataFile = h5py.Empty(int) #to close the previous instance when working in ipython

# temperatureColormap = cmocean.cm.balance
temperatureColormap = plt.cm.coolwarm

plt.rcParams['text.usetex'] = True
plt.rcParams['text.latex.preamble'] = r"\usepackage{amsmath}"

fig, ax = plt.subplots(1, 2, gridspec_kw={'width_ratios': [1, 1]}, figsize=(10, 5.1))

# case sketch
width, height = 1, 1

colors = temperatureColormap([0, 1-1e-15])
ax[0].plot([0, 0], [0, height], '-', linewidth=3, clip_on=False, color=colors[0])
ax[0].plot([width, width], [0, height], '-', linewidth=3, clip_on=False, color=colors[1])
for h in [0, height]:
    ax[0].plot([0, width], [h, h], 'k-', linewidth=3, clip_on=False)

ax[0].text(0.025, 0.55*height, "$T = T_C$", fontsize="large")
ax[0].text(0.025, 0.45*height, r"$\boldsymbol{v}=0$", fontsize="large")

ax[0].text(width - 0.225, 0.55*height, "$T = T_H$", fontsize="large", ha="left")
ax[0].text(width - 0.225, 0.45*height, r"$\boldsymbol{v}=0$", fontsize="large", ha="left")


ax[0].text(0.5*width, 0.05, r"$\frac{\partial T}{\partial{\boldsymbol{n}}} = 0$ \qquad $\boldsymbol{v}=0$",
           fontsize="large", ha="center", va="bottom")
ax[0].text(0.5*width, height - 0.05, r"$\frac{\partial T}{\partial{\boldsymbol{n}}} = 0$ \qquad $\boldsymbol{v}=0$",
           fontsize="large", ha="center", va="top")

ax[0].set_aspect("equal")
ax[0].set_xticks([0, width])
ax[0].set_xticklabels([0, "L"], fontsize="large")
ax[0].set_yticks([0, height])
ax[0].set_yticklabels([0, "L"], fontsize="large")
ax[0].set_frame_on(False)

ax[0].set_ylabel("$y$", fontsize="large")
ax[0].set_xlabel("$x$", fontsize="large")
# ax[0].set_title("Boundary conditions")

angles = np.linspace(0, 2*np.pi, 5000)
x = 0.5 + 0.25*np.cos(angles)
y = 0.5 + 0.25*np.sin(angles)
points = np.array([x, y]).T.reshape(-1, 1, 2)
segments = np.concatenate([points[:-1], points[1:]], axis=1)
norm = plt.Normalize(x.min(), x.max())
lc = collections.LineCollection(segments, colors=temperatureColormap(norm(x)))
lc.set_linewidth(6)
lc.set_alpha(0.9)
ax[0].add_collection(lc)


width = 0.04
move = width / 2 / np.sqrt(2)
for intersect in [(0.225, 0.225), (0.775, 0.225), (0.775, 0.775), (0.225, 0.775)]:
    dirx = 1 if intersect[1] <= 0.5 else -1
    diry = 1 if intersect[0] >= 0.5 else -1
    idx = np.argmin(np.abs(x - intersect[0]) + np.abs(y - intersect[1]))
    c = temperatureColormap(norm(x[idx]))
    ax[0].arrow(x[idx] - dirx * move, y[idx] - diry * move, dirx * 1e-5, diry * 1e-5,
                head_width=width, head_length=width, head_starts_at_zero=False,
                edgecolor=c, facecolor=c)

ax[0].arrow(0.5, 0.6, 0, -0.2, width=0.004, color="k", length_includes_head=True)
ax[0].text(0.52, 0.5, r"$\boldsymbol{g}$", fontsize="large")

# viscosity

def viscosity(grad, n):
    return np.power(grad, n-1)

grad = np.linspace(1e-10, 10, 1000)
for n in [0.6, 0.7, 0.8, 0.9, 1]:
    ax[1].plot(grad, viscosity(grad, n), label=f"$n={n}$", linewidth=1.5)
ax[1].legend()
ax[1].set_xlabel(r"$\sqrt{\frac{1}{2} || \nabla \boldsymbol{v} + (\nabla \boldsymbol{v})^T||}$", fontsize="large")
ax[1].set_ylabel("$\eta / \eta_0$", fontsize="large")
ax[1].set_ylim(0.4, 1.4)
ax[1].grid(linestyle="-")

plt.tight_layout()

fig.savefig("../figures/caseDVD.pdf")
fig.savefig("../figures/caseDVD", dpi=300)