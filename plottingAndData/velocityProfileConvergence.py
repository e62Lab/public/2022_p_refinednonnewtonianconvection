# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
import json
import gzip
import scipy.interpolate as interpolate
import matplotlib.ticker as mticker
import cmocean

plt.rc('text', usetex = False)
#plt.rc('font', size = 14, family = 'serif', serif = ['Arial'])
plt.rc('font', size = 14, family = 'serif', serif = ['STIXGeneral'])
plt.rc('xtick', labelsize = 'small')
plt.rc('ytick', labelsize = 'small')
plt.rc('legend', frameon = True, fontsize = 'medium', loc = "best")
plt.rc('figure', figsize = (10, 6), facecolor = "white")
plt.rc('lines', linewidth=1.0)
plt.rc('axes', xmargin = 0, ymargin = 0, zmargin=0, autolimit_mode = 'round_numbers')
plt.rc('mathtext', fontset = 'cm', rm = 'serif')

plt.close('all')

def toScientific(number, decimals = 1):
    try:
        if number == np.inf:
            return "$\infty$"
        exponent = int(np.floor(np.log10(np.abs(number))))
        if decimals < 0:
            return f"$10^{ {exponent} }$"
        elif exponent == 0:
            
            return str(np.round(number, decimals))
        elif exponent == 1:
            return str(np.round(number, decimals))
        elif exponent == 2:
            return str(np.round(number, decimals))
        elif exponent == -1:
            return str(np.round(number, decimals+1))
        elif exponent == -2:
            return str(np.round(number, decimals+2))
        else:
            return f"${np.round(number / np.power(10.0, exponent), decimals)} \cdot 10^{ {exponent} }$"
    except Exception as e:
        print("Got exception: ", e)
        return "NaN"

#------------------------------------------------------------------------------
with gzip.open("plotData/monOrderConvergence.json.gz", 'r') as zipfile:
    data = json.load(zipfile)

# %% Convergence of interpolated velocity profiles nex to the vertical boundary
Ra = int(1e6)
Pr = 100
n = 0.6
monOrder = 2

min_time = 9.9
cross_location = 0.6
interp_count = 2000
includeBoundary = True
xlim = [0.9, 1]

candidates = {}
for name, contents in data.items():
    attrs = contents["attrs"]
    if (attrs["dimensionless.Ra"] == Ra and attrs["dimensionless.Pr"] == Pr
            and attrs["num.mon_order"] == monOrder and attrs["phy.power_index"] == n):
        candidates[attrs["num.h"]] = contents

discretizations = sorted(candidates.keys(), reverse=True)

fig, ax = plt.subplots(figsize=(10, 4))

fig3d = plt.figure()
ax3d = fig3d.add_subplot(projection='3d')

colors = iter(cmocean.cm.amp(np.linspace(0.3, 1, len(discretizations))))
for h in discretizations:
    c = next(colors)
    contents = candidates[h]
    attrs = contents["attrs"]    
    if contents["t"][-1] < min_time: continue
    pos = np.array(contents["pos"])
    N = contents["N"]
    vy = np.array(contents["v"][1])
    vy = vy * attrs["phy.c_p"] * attrs["phy.rho"] * attrs["case.height"] / attrs["phy.lam"]
    
    
    # interp_loc = np.array([np.linspace(*xlim, interp_count), np.ones(interp_count) * cross_location]).T
    # if includeBoundary:
    #     relevant_indices = ((pos[1] > cross_location - 5 * h) * (pos[1] < cross_location + 5 * h))
    # else:
    #     relevant_indices = ((pos[1] > cross_location - 5 * h) * (pos[1] < cross_location + 5 * h)
    #                         * (pos[0] >= h / 2) * (pos[0] <= 1 - h / 2))
    # interp_val = interpolate.griddata(pos[:, relevant_indices].T, vy[relevant_indices], interp_loc,
    #                                   method="cubic")
    if includeBoundary:
        closest_indices = np.nonzero((pos[1] >= cross_location - h/2) * (pos[1] <= cross_location + h/2)
                           * (pos[0] >= xlim[0]) * (pos[0] <= xlim[1]))[0]
    else:
        closest_indices = np.nonzero((pos[1] >= cross_location - h/2) * (pos[1] <= cross_location + h/2)
                           * (pos[0] >= xlim[0]) * (pos[0] <= xlim[1]) * (pos[0] >= h / 2) * (pos[0] <= 1 - h / 2))[0]
    closest_indices = closest_indices[np.argsort(pos[0, closest_indices])]
    spl = interpolate.UnivariateSpline(pos[0, closest_indices], vy[closest_indices], s=100)
    interp_loc = np.linspace(*xlim, interp_count)
    interp_val = spl(interp_loc)
    ax.plot(interp_loc, interp_val, color=c, label=f"$N={N}$")
    ax3d.plot(np.ones_like(interp_loc) * np.log10(N), interp_loc, interp_val, color=c)
    ax3d.scatter(np.ones_like(pos[0, closest_indices]) * np.log10(N), pos[0, closest_indices], vy[closest_indices], s=10, color=[c for i in closest_indices])
    ax.plot(pos[0, closest_indices], vy[closest_indices], ".", color=c)

ax.set_ylabel(r"$v_y$")
ax.grid(linestyle="-")

# plt.suptitle(f"Vertical velocity at $y={cross_location}$ for Ra$={Ra}$, Pr$={Pr}$ and support size {support}")
ax.set_xlabel(r"$x$")
ax.legend(ncol=3)
ax.set_xlim(xlim[0] + 0.05, xlim[1])

plt.tight_layout()

# fig.savefig("../figures/velocityProfileConvergence.pdf")
# fig.savefig("../figures/velocityProfileConvergence", dpi=300)


# %% 3d visualisation
fig3d = plt.figure(figsize=(12, 5))
lsp = np.linspace(3e-2, 1e-3, 3)
Ra = int(1e4)
Pr = 100
n = 1
#Plot the not really connected but present convergence plot first
plot_data = {}
for filename, contents in data.items():
    attrs = contents["attrs"]
    if attrs["phy.power_index"] == n and attrs["dimensionless.Ra"] == Ra:
        nusselts = contents["Nu"]
        d = plot_data.setdefault(attrs["num.mon_order"], [])
        d.append([attrs["num.h"], nusselts[-1]])
            
ax = fig3d.add_subplot(1, 5, 1)
for key, values in sorted(plot_data.items(), key=lambda x: x[0]):
    values = sorted(values, key=lambda x: 1/x[0])
    arrVal = np.array(values[:-1]).T
    arrVal[1] = np.abs(arrVal[1] - values[-1][1])
    ax.plot(*arrVal, '.--', label=f"$m={key:.0f}$")
ax.plot(lsp, np.power(3 * lsp, 2), 'k:', alpha=0.8, label="$\mathcal{O}(h^2)$")
ax.plot(lsp, np.power(60 * lsp, 4), 'k-.', alpha=0.8, label="$\mathcal{O}(h^4)$")
ax.grid(linestyle="-")
ax.set_xscale("log")
ax.set_yscale("log")
ax.xaxis.set_inverted(True)
ax.set_xlim(3e-2, 1e-3)
ax.set_ylim(1e-6, 1e-1)
ax.set_xlabel("$h$")
ax.set_ylabel(r"$| \overline{\mathrm{Nu}} - \overline{\mathrm{Nu}}_{h_{\mathrm{min}}}|$")
ax.set_title(f"Ra$=10^{{{np.log10(Ra):g}}}$, $n = {n}$")
# ax.legend(loc="upper right")
ax.legend()

# Plot the 3d intersections
Pr = 100
n = 0.6
monOrder = 2

min_time = 9.9
cross_location = 0.6
interp_count = 2000
includeBoundary = True
xlim = [0.9, 1]

col = 2
for Ra in [int(1e5), int(1e6)]:
    candidates = {}
    for name, contents in data.items():
        attrs = contents["attrs"]
        if (attrs["dimensionless.Ra"] == Ra and attrs["dimensionless.Pr"] == Pr
                and attrs["num.mon_order"] == monOrder and attrs["phy.power_index"] == n):
            candidates[attrs["num.h"]] = contents
    
    discretizations = sorted(candidates.keys(), reverse=True)
    
    ax3d = fig3d.add_subplot(1, 5, (col, col+1), projection='3d')
    col += 2
    ax3d.xaxis.pane.fill = False
    ax3d.yaxis.pane.fill = False
    ax3d.zaxis.pane.fill = False
    ax3d.xaxis.pane.set_edgecolor('w')
    ax3d.yaxis.pane.set_edgecolor('w')
    
    ax3d.zaxis.pane.set_edgecolor('w')
    ax3d.view_init(elev=20, azim=240, roll=0)
    colors = iter(cmocean.cm.amp(np.linspace(1, 0.3, len(discretizations))))
    
    # Remove one from 1e5 to match the number of plots
    if Ra == 100000:
        discretizations = discretizations[1:]
    
    maxy = 0
    for h in discretizations:
        c = next(colors)
        contents = candidates[h]
        attrs = contents["attrs"]    
        if contents["t"][-1] < min_time: continue
        pos = np.array(contents["pos"])
        N = contents["N"]
        vy = np.array(contents["v"][1])
        vy = vy * attrs["phy.c_p"] * attrs["phy.rho"] * attrs["case.height"] / attrs["phy.lam"]
        
        if includeBoundary:
            closest_indices = np.nonzero((pos[1] >= cross_location - h/2) * (pos[1] <= cross_location + h/2)
                               * (pos[0] >= xlim[0]) * (pos[0] <= xlim[1]))[0]
        else:
            closest_indices = np.nonzero((pos[1] >= cross_location - h/2) * (pos[1] <= cross_location + h/2)
                               * (pos[0] >= xlim[0]) * (pos[0] <= xlim[1]) * (pos[0] >= h / 2) * (pos[0] <= 1 - h / 2))[0]
        closest_indices = closest_indices[np.argsort(pos[0, closest_indices])]
        spl = interpolate.UnivariateSpline(pos[0, closest_indices], vy[closest_indices], s=100)
        interp_loc = np.linspace(*xlim, interp_count)
        interp_val = spl(interp_loc)
        maxy = max(maxy, max(interp_val))
        ax3d.plot(np.ones_like(interp_loc) * np.log10(N), interp_loc, interp_val, color=c)
        ax3d.scatter(np.ones_like(pos[0, closest_indices]) * np.log10(N), pos[0, closest_indices], vy[closest_indices], s=10, color=[c for i in closest_indices])
    ax3d.set_zlim(0, maxy)
    ax3d.set_xlabel("$N$")
    ax3d.yaxis.set_inverted(True)
    ax3d.set_ylabel("$x$")
    ax3d.set_zlabel("$v_y$")
    ax3d.set_title(f"Ra$=10^{{{np.log10(Ra):g}}}$", y=1)
    
    # My axis should display 10⁻¹ but you can switch to e-notation 1.00e+01
    def log_tick_formatter(val, pos=None):
        return f"$10^{{{val:g}}}$"
    ax3d.xaxis.set_major_formatter(mticker.FuncFormatter(log_tick_formatter))
    ax3d.xaxis.set_major_locator(mticker.MaxNLocator(integer=True))




fig3d.tight_layout()

# fig3d.savefig("../figures/velocityProfileConvergence3d.pdf")
# fig3d.savefig("../figures/velocityProfileConvergence3d", dpi=300)