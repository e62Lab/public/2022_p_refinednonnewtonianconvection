# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
import h5py
import os
import json
import gzip


plt.rc('text', usetex = False)
#plt.rc('font', size = 14, family = 'serif', serif = ['Arial'])
plt.rc('font', size = 14, family = 'serif', serif = ['STIXGeneral'])
plt.rc('xtick', labelsize = 'small')
plt.rc('ytick', labelsize = 'small')
plt.rc('legend', frameon = True, fontsize = 'medium', loc = "best")
plt.rc('figure', figsize = (10, 6), facecolor = "white")
plt.rc('lines', linewidth=1.0)
plt.rc('axes', xmargin = 0, ymargin = 0, autolimit_mode = 'round_numbers')
plt.rc('mathtext', fontset = 'cm', rm = 'serif')

plt.close('all')

#------------------------------------------------------------------------------

with gzip.open("plotData/nodePlacement.json.gz", 'r') as zipfile:
    data = json.load(zipfile)
    
#----------------- Unrefined vs refined vs porous node visualisation
fig, ax = plt.subplots(1, 3, sharey=True, figsize=(10, 4))
ax[0].scatter(*data["constant"]["pos"], s=0.5, color='k')
ax[1].scatter(*data["refined"]["pos"], s=0.5, color='k')
ax[2].scatter(*data["porous"]["pos"], s=0.25, color='k', linewidths=0)

for a in ax:
    a.set_aspect("equal")
    a.set_xlabel(r"$x$")
    a.set_frame_on(False)
ax[0].set_ylabel("$y$")

fig.tight_layout()

fig.savefig("../figures/nodePlacement", dpi=300)



