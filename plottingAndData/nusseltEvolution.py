# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
import json
import gzip

plt.rc('text', usetex = False)
#plt.rc('font', size = 14, family = 'serif', serif = ['Arial'])
plt.rc('font', size = 14, family = 'serif', serif = ['STIXGeneral'])
plt.rc('xtick', labelsize = 'small')
plt.rc('ytick', labelsize = 'small')
plt.rc('legend', frameon = True, fontsize = 'medium', loc = "best")
plt.rc('figure', figsize = (10, 6), facecolor = "white")
plt.rc('lines', linewidth=1.0)
plt.rc('axes', xmargin = 0, ymargin = 0, autolimit_mode = 'round_numbers')
plt.rc('mathtext', fontset = 'cm', rm = 'serif')

plt.close('all')

def toScientific(number, decimals = 1):
    try:
        if number == np.inf:
            return "$\infty$"
        exponent = int(np.floor(np.log10(np.abs(number))))
        if decimals < 0:
            return f"$10^{ {exponent} }$"
        elif exponent == 0:
            return str(np.round(number, decimals))
        elif exponent == 1:
            return str(np.round(number, decimals))
        elif exponent == 2:
            return str(np.round(number, decimals))
        elif exponent == -1:
            return str(np.round(number, decimals+1))
        elif exponent == -2:
            return str(np.round(number, decimals+2))
        else:
            return f"${np.round(number / np.power(10.0, exponent), decimals)} \cdot 10^{ {exponent} }$"
    except Exception as e:
        print("Got exception: ", e)
        return "NaN"

#------------------------------------------------------------------------------
with gzip.open("plotData/timestepEvolution.json.gz", 'r') as zipfile:
    data = json.load(zipfile)


# %% Nusselt and timestep plot

print(f"Available h: {list(data.keys())}")
h = "0.005"

plotEvery = 1
fig, ax = plt.subplots(3, 2, sharex=True, figsize=(10, 5.5))
ylims = iter([10, 27, 70])
row = 0
maxX = 0
for Ra in sorted(data[h].keys()):
    for n in sorted(data[h][Ra].keys()):
        d = data[h][Ra][n]
        ax[row, 0].plot(d["t"], d["nusselt"], label = f"$n = {n}$")
        ax[row, 1].plot(d["t"][::plotEvery], d["dt"][::plotEvery], label = f"$n = {n}$")
        maxX = max(maxX, max(d["t"]))
    ax[row, 0].set_ylim(0, next(ylims))
    ax[row, 0].set_ylabel(f"Ra$=${toScientific(float(Ra), -1)}\n" + r"$\overline{\mathrm{Nu}}$")
    ax[row, 1].set_ylabel(r"$\Delta t$")
    handles, labels = ax[row, 0].get_legend_handles_labels()
    ax[row, 0].legend([handles[row]], [labels[row]])
    for a in ax[row]:
        a.grid(linestyle="-")
    row += 1

ax[0, 0].set_xlim(0, maxX)
for a in ax[-1]:
    a.set_xlabel("$t$")


fig.tight_layout()

fig.savefig("../figures/nusseltEvolution.pdf")
fig.savefig("../figures/nusseltEvolution", dpi=300)
