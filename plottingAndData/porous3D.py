# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import matplotlib.tri as triang
import numpy as np
import json
import gzip
import cmocean
from mayavi import mlab
import scipy.spatial as spatial
from mpl_toolkits.axes_grid1 import make_axes_locatable


plt.rc('text', usetex = False)
#plt.rc('font', size = 14, family = 'serif', serif = ['Arial'])
plt.rc('font', size = 14, family = 'serif', serif = ['STIXGeneral'])
plt.rc('xtick', labelsize = 'small')
plt.rc('ytick', labelsize = 'small')
plt.rc('legend', frameon = True, fontsize = 'medium', loc = "best")
plt.rc('figure', figsize = (10, 6), facecolor = "white")
plt.rc('lines', linewidth=1.0)
plt.rc('axes', xmargin = 0, ymargin = 0, autolimit_mode = 'round_numbers')
plt.rc('mathtext', fontset = 'cm', rm = 'serif')

plt.close('all')
mlab.close(all=True)

#------------------------------------------------------------------------------
with gzip.open("plotData/porousCases.json.gz", 'r') as zipfile:
    data = json.load(zipfile)

# filename = "3D_porous_verticalRa1e6n09.h5"
filename = "3D_porous_verticalRa1e6n08.h5"
# filename = "3D_porous_verticalRa1e5n07.h5"

d = data[filename]
pos = np.array(d["pos"])
v = np.array(d["v"])
T = np.array(d["T"])
v = v * d["attrs"]["phy.c_p"] * d["attrs"]["phy.rho"] * d["attrs"]["case.height"] / d["attrs"]["phy.lam"]
h = d["attrs"]["num.h"]

outFile = f"../figures/porous3D_Ra1e{int(np.log10(d['attrs']['dimensionless.Ra']))}n{str(d['attrs']['phy.power_index']).replace('.','')}"

# %% porous3D - 3d visualisation of a vertical convection with obstructions
mlab.figure(fgcolor=(0., 0., 0.), bgcolor=(1, 1, 1), size=(1000, 1000))
maxT = 1
maxV = np.max(np.sqrt(np.sum(v**2, axis=0)))
qvr = mlab.quiver3d(*pos, *v, mask_points=1, color=(0, 0, 0), mode="arrow", scale_factor=0.125/maxV,
                    scalars=T, vmin=-maxT, vmax=maxT, opacity=1)
qvr.glyph.color_mode = 'color_by_scalar'
qvr.module_manager.scalar_lut_manager.lut.table = cmocean.cm.balance(np.arange(256)) * 255
mlab.outline()


obstructions = np.nonzero((np.sqrt(np.sum(v**2, axis=0)) == 0)
                          * np.all(pos != 0, axis=0) * np.all(pos != 1, axis=0))[0]

obstruction_pos =  pos[:, obstructions].T
kdtree = spatial.KDTree(obstruction_pos)
pairs = kdtree.query_pairs(d["attrs"]["case.min_sphere_distance"])
print(len(pairs))

connected = []
for (first, second) in pairs:
    for i in range(len(connected)):
        if first in connected[i]:
            for j in range(i+1, len(connected)):
                if second in connected[j]:
                    connected[i].update(connected.pop(j))
                    break
            else:
                connected[i].add(second)
        elif second in connected[i]:
            for j in range(i+1, len(connected)):
                if first in connected[j]:
                    connected[i].update(connected.pop(j))
                    break
            else:
                connected[i].add(first)
        else:
            continue
        break
    else:
        connected.append({first, second})

for i in connected:
    if len(obstruction_pos[list(i)]) >= 4:
        hull = spatial.ConvexHull(obstruction_pos[list(i)])
        
        # calculates the simplex edge length and discards those that have two edges significantly longer than the discretisation size
        # maskTooLarge = (np.sort(np.linalg.norm(hull.points[hull.simplices]
        #                              - hull.points[np.roll(hull.simplices, 1, axis=1)], axis=-1), axis=-1)[:, 1]
        #                 <= 2*h)
        maskTooLarge = np.arange(len(hull.simplices))
        
        mlab.triangular_mesh(*hull.points.T, hull.simplices[maskTooLarge], color=(0.86, 0.86, 0.86), opacity=1,
                              representation="surface")
        # mlab.triangular_mesh(*hull.points.T, hull.simplices[maskTooLarge], color=(0, 0, 0), opacity=0.2,
                             # representation="wireframe")

print(mlab.view())
mlab.screenshot()
base_angle = 90
offset = 50
angles = [base_angle + offset, base_angle, base_angle - offset]
zooms = [2.585, 2.38, 2.585]
images = []
for i in range(len(angles)):
    mlab.view(angles[i], 90, zooms[i], "auto")
    images.append(mlab.screenshot(mode="rgb"))

fig, ax = plt.subplots(1, len(images), figsize=(10, 3.5))
for i in range(len(images)):
    ax[i].imshow(images[i])
    ax[i].set_axis_off()
fig.tight_layout()

fig.savefig(outFile, dpi=300)
fig.savefig(outFile + ".pdf", dpi=300)

# %% porous3Dintersection - 3d obstructed case intersections
markerSize = 5
intersection_location = 0.5
intersection_index_list = [0, 1, 2]
axis_names = ["x", "y", "z"]
vy = v[2]
viscosity = np.array(d["viscosity"])
mu = d["attrs"]["phy.mu"]

scatter = True
viscLog = False
colorLevels = 20

if viscLog:
    viscosity = np.log(mu / viscosity)
else:
    viscosity = mu / viscosity

maxV = max(abs(min(vy)), max(vy))
velLevels = np.linspace(-maxV, maxV, colorLevels)
TLevels = np.linspace(-maxT, maxT, colorLevels)
viscLevels = np.linspace(min(viscosity), max(viscosity), colorLevels)

fig, ax = plt.subplots(3, len(intersection_index_list), sharex=True, figsize=(10, 8))

i = 0
for intersection_index in intersection_index_list:
    relevant_indices = ((pos[intersection_index] > intersection_location - h / 2) * (pos[intersection_index] < intersection_location + h / 2))
    planePos = pos[[idx for idx in intersection_index_list if idx != intersection_index]][:, relevant_indices]
    
    tri = triang.Triangulation(*planePos)
    t = tri.triangles
    mask = np.any(np.sqrt((tri.x[t] - tri.x[np.roll(t, 1, axis=1)])**2 + (tri.y[t] - tri.y[np.roll(t, 1, axis=1)])**2)
                    > 3*h, axis=1)
    tri.set_mask(mask)
    
    if scatter:
        img = ax[0, i].scatter(*planePos, s=markerSize, c=vy[relevant_indices], cmap=cmocean.cm.balance, vmin=-maxV, vmax=maxV)
    else:
        img = ax[0, i].tricontourf(tri, vy[relevant_indices], cmap=cmocean.cm.balance, levels=velLevels)
    if i == 2:
        divider = make_axes_locatable(ax[0, -1])
        cax = divider.append_axes("right", "7%", pad="15%")
        fig.colorbar(img, cax=cax, label=r"$v_y$")
    if scatter:
        img = ax[1, i].scatter(*planePos, s=markerSize, c=T[relevant_indices], cmap=cmocean.cm.balance, vmin=min(T), vmax=max(T))
    else:
        img = ax[1, i].tricontourf(tri, T[relevant_indices], cmap=cmocean.cm.balance, levels=TLevels)
    if i == 2:
        divider = make_axes_locatable(ax[1, -1])
        cax = divider.append_axes("right", "7%", pad="15%")
        fig.colorbar(img, cax=cax, label=r"$T$")
    if scatter:
        img = ax[2, i].scatter(*planePos, s=markerSize, c=viscosity[relevant_indices], cmap=cmocean.cm.amp, vmin=min(viscosity), vmax=max(viscosity))
    else:
        img = ax[2, i].tricontourf(tri, viscosity[relevant_indices], cmap=cmocean.cm.balance, levels=viscLevels)
    if i == 2:
        divider = make_axes_locatable(ax[2, -1])
        cax = divider.append_axes("right", "7%", pad="15%")
        if viscLog:
            fig.colorbar(img, cax=cax, label=r"$\ln(\frac{\mu_0}{\mu})$")
        else:
            fig.colorbar(img, cax=cax, label=r"$\frac{\mu_0}{\mu}$")
        
    for j in [0, 1, 2]:
        ax[j, i].set_aspect("equal")
    ax[0, i].set_title(f"${axis_names[intersection_index]} = {intersection_location}$")
    x_set = False
    for j in [0, 1, 2]:
        if j == intersection_index: continue
        if not x_set:
            ax[-1, i].set_xlabel(f"${axis_names[j]}$")
            x_set = True
        else:
            for jj in [0, 1, 2]:
                ax[jj, i].set_ylabel(f"${axis_names[j]}$")
    
    i += 1

fig.tight_layout()

fig.savefig(outFile + "_intersections", dpi=300)
# fig.savefig(outFile + "_intersections.pdf", dpi=300)