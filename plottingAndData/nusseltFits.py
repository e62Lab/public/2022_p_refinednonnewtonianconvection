# -*- coding: utf-8 -*-

import numpy as np

def turan(n, Ra, Pr):
    if np.any(n <= 1):
        c1 = 1.343
        c2 = 0.065
        c3 = 0.036
    else:
        c1 = 0.858
        c2 = 0.071
        c3 = 0.034
    b = c1 * np.power(Ra, c2) * np.power(Pr, c3)
    return (0.162 * np.power(Ra, 0.043) * np.power(Pr, 0.341) / np.power(1 + Pr, 0.091)
            * np.power(np.power(Ra, 2 - n) / np.power(Pr, n), 1 / (2 * n + 2))
            * np.exp(b * (n - 1)))

def kim(n, Ra, Pr):
    return 0.3 * np.power(n, 0.4) * np.power(Ra, 1 / (3 * n + 1))