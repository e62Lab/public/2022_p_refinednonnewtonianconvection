# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
import h5py
import os
import json
import gzip


plt.rc('text', usetex = False)
#plt.rc('font', size = 14, family = 'serif', serif = ['Arial'])
plt.rc('font', size = 14, family = 'serif', serif = ['STIXGeneral'])
plt.rc('xtick', labelsize = 'small')
plt.rc('ytick', labelsize = 'small')
plt.rc('legend', frameon = True, fontsize = 'medium', loc = "best")
plt.rc('figure', figsize = (10, 6), facecolor = "white")
plt.rc('lines', linewidth=1.0)
plt.rc('axes', xmargin = 0, ymargin = 0, autolimit_mode = 'round_numbers')
plt.rc('mathtext', fontset = 'cm', rm = 'serif')

plt.close('all')

#------------------------------------------------------------------------------
jsonPath = "plotData/refinementScheme.json.gz"
with gzip.open(jsonPath, 'r') as zipfile:
    file_index = json.load(zipfile)
# %% refinementScheme - schematis representation of refinement and node count comparison

fig, ax = plt.subplots(ncols=2, figsize=(10, 4), gridspec_kw={"width_ratios" : [1, 1.5]})
#--------------- Refinement scheme

plotx = [0, 0.1, 0.5]
ploty = [1, 1, 10]


ax[0].plot(plotx, ploty, 'k-')
ax[0].annotate(text='$w$', xy=(0.04,1.5), fontsize="large")
ax[0].annotate(text='', xy=(0.1,1.3), xytext=(0,1.3), arrowprops=dict(arrowstyle='<->'))
ax[0].set_xlabel(r"$\frac{x}{L}$", fontsize="large")
ax[0].set_ylabel(r"Internodal distance", fontsize="large")
ax[0].set_yticks([1, 10])
ax[0].set_yticklabels(["$h_1$", "$h_2$"], fontsize="large")
ax[0].set_ylim(0.5, 10.5)


#----------------- Refined vs constant node count
xMin, xMax = 1, 0
for refine in [0, 1]:
    plotx, ploty = [], []
    for file, attrs in file_index.items():
        if attrs["variable_density"] == refine:
            plotx.append(attrs["h"])
            ploty.append(attrs["N"])
            if refine:
                d = attrs["dense_band"]
                hMax = attrs["max_step"]
                ratio = attrs["refine_ratio"]
    if refine:
        label = f"Refined with $w = {d}$, " + r"$\frac{h_2}{h_1}" + f" = {ratio:.0f}$ & " + r"$h_{\mathrm{max}}" + f" = {hMax}$"
    else:
        label = "Constant density"
    order = np.argsort(plotx)[::-1]
    plotx = [plotx[i] for i in order]
    ploty = [ploty[i] for i in order]
    ax[1].plot(plotx, ploty, '.--', label=label)
    xMin = min(xMin, min(plotx))
    xMax = max(xMax, max(plotx))

ax[1].legend()
ax[1].set_xscale("log")
ax[1].set_yscale("log")
ax[1].set_xlim(xMin, xMax)
ax[1].set_xlabel(r"$h_{\mathrm{min}}$")
ax[1].set_ylabel("$N$")
ax[1].grid(linestyle="-", which="major")
ax[1].grid(linestyle="-", which="minor", alpha=0.5)

fig.tight_layout()

fig.savefig("../figures/refinementScheme.pdf")
fig.savefig("../figures/refinementScheme", dpi=300)


# %% refinementNodes - refinement visualisation (disused)
for file, attrs in file_index.items():
    if "pos" in attrs:
        pos = attrs["pos"]
        break
fig, ax = plt.subplots(figsize=(4, 4))
ax.scatter(*pos, s=0.5, color='k')
ax.set_aspect("equal")
ax.set_xlabel(r"$x$")
ax.set_ylabel("$y$")

fig.tight_layout()

fig.savefig("../figures/refinementNodes", dpi=300)



