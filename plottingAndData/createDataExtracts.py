# -*- coding: utf-8 -*-
import os
import numpy as np
import h5py
import json
import gzip

def storeJSON(outName, data):
    if len(data) > 0:
        with gzip.open(outName, 'wt', encoding="utf-8") as zipfile:
            json.dump(data, zipfile)
        print(outName + " created")

def dirToJSON(dirPath, outName):
    data = {}
    for filename in os.listdir(dirPath):
        if filename.endswith(".h5"):
            try:
                with h5py.File(os.path.join(dirPath, filename), "r") as dataFile:
                    steps = sorted(list(dataFile["/values"].keys()), key=lambda x: int(x.replace("step", "")))
                    data[filename] = {}
                    data[filename]["attrs"] = {k : v if type(v) is not np.bytes_ else str(v) for k, v in dataFile.attrs.items()}
                    data[filename]["pos"] = dataFile["positions"][()].tolist()
                    data[filename]["v"] = dataFile[f"/values/{steps[-1]}/v"][()].tolist()
                    data[filename]["T"] = dataFile[f"/values/{steps[-1]}/T"][()].tolist()
                    data[filename]["N"] = len(data[filename]["T"])
                    data[filename]["t"] = [dataFile[f"/values/{s}"].attrs["time"] for s in steps]
                    data[filename]["Nu"] = [dataFile[f"/values/{s}"].attrs["nusselt"] for s in steps]
                    if "viscosity" in dataFile:
                        data[filename]["viscosity"] = dataFile["viscosity"][()].tolist()
                    if "divergence" in dataFile:
                        data[filename]["divergence"] = dataFile["divergence"][()].tolist()
            except (KeyError, OSError, RuntimeError) as e:
                print(f"Reading {filename} failed with {e}")
    storeJSON(outName, data)

# %% Refine ratio impact for different support sizes
path = "../code/data/"
outName = "plotData/refineRateSupportSize.json.gz"

data = {}
for case in ["e4", "e5", "e6"]:
    #TODO: Fix/remove this workaround
    additional = ""
    if case == "e4":
        additional = "_n1"
    folder = f"refineRate_{case}{additional}"
    for file in os.listdir(os.path.join(path, folder)):
        if file.endswith(".h5"):
            try:
                with h5py.File(os.path.join(path, folder, file), "r") as dataFile:
                    steps = sorted(list(dataFile["/values"].keys()), key=lambda x: int(x.replace("step", "")))
                    endTime = dataFile[f"/values/{steps[-1]}"].attrs["time"]
                    if endTime < 0.9 * dataFile.attrs["case.end_time"]:
                        continue
                    
                    cross_location = 0.5
                    includeNodesCloserThan = 5
                    h = dataFile.attrs["num.h"]
                    pos = dataFile["positions"][()]
                    closest_indices = np.nonzero((pos[1] >= cross_location - includeNodesCloserThan * h) * (pos[1] <= cross_location + includeNodesCloserThan * h))[0]
                    closest_indices = closest_indices[np.argsort(pos[0, closest_indices])]
                    vy = dataFile[f"/values/{steps[-1]}/v"][()][1, closest_indices]
                    vy = vy * dataFile.attrs["phy.c_p"] * dataFile.attrs["phy.rho"] * dataFile.attrs["case.height"] / dataFile.attrs["phy.lam"]


                    if case not in data:
                        data[case] = {}
                    data[case][file] = {"h" : dataFile.attrs["num.h"],
                                        "support_size_factor" : dataFile.attrs["num.support_size_factor"],
                                        "mon_order" : dataFile.attrs["num.mon_order"],
                                        "attrs" : {k : v if type(v) is not np.bytes_ else str(v) for k, v in dataFile.attrs.items()},
                                        "refine_ratio" : dataFile.attrs["fill.refine_ratio"] if "fill.refine_ratio" in dataFile.attrs else dataFile.attrs["fill.max_step"] / dataFile.attrs["num.h"],
                                        "nusselt" : dataFile[f"/values/{steps[-1]}"].attrs["nusselt"],
                                        "maxDiv" : np.max(np.abs(dataFile["divergence"])),
                                        "crossPosX" : pos[0, closest_indices].tolist(),
                                        "crossVy" : vy.tolist(),
                                        "N" : pos.shape[-1]}
            except (KeyError, OSError, RuntimeError) as e:
                print(f"Reading {file} failed with {e}")
storeJSON(outName, data)
# %% Porous
outName = "plotData/porousCases.json.gz"
path = "../code/data/porous/"
filenames = ["2D_porous_central06_n06.h5", "2D_porous_central06_n1.h5",
             "3D_porous_verticalRa1e6n09.h5", "3D_porous_verticalRa1e6n08.h5", "3D_porous_verticalRa1e5n07.h5"]

try:
    data = {}
    for filename in filenames:
        with h5py.File(os.path.join(path, filename), "r") as dataFile:
            steps = [i for i in dataFile["/values"].keys() if "step" in i]
            steps = sorted(steps, key=lambda x: int(x.replace("step", "")))
            data[filename] = {}
            data[filename]["attrs"] = {k : v if type(v) is not np.bytes_ else str(v) for k, v in dataFile.attrs.items()}
            data[filename]["pos"] = dataFile["positions"][()].tolist()
            data[filename]["v"] = dataFile[f"/values/{steps[-1]}/v"][()].tolist()
            data[filename]["T"] = dataFile[f"/values/{steps[-1]}/T"][()].tolist()
            data[filename]["viscosity"] = dataFile["viscosity"][()].tolist()
            data[filename]["t"] = [dataFile[f"/values/{s}"].attrs["time"] for s in steps]
            data[filename]["Nu"] = [dataFile[f"/values/{s}"].attrs["nusselt"] for s in steps]
    storeJSON(outName, data)
except Exception as e:
    print("Porous data extraction failed with:")
    print(e)

# %% nodePlacement
outName = "plotData/nodePlacement.json.gz"
path = "../code/data/"
filenames = [("constant", "sizes/h0_0125variable_density0.h5"),
             ("refined", "sizes/h0_005variable_density1.h5"),
             ("porous", "porous/2D_porous_central06_n1.h5")]
try:
    data = {}
    for tag, filename in filenames:
        with h5py.File(os.path.join(path, filename), "r") as dataFile:
            steps = sorted(list(dataFile["/values"].keys()), key=lambda x: int(x.replace("step", "")))
            data[tag] = {}
            data[tag]["attrs"] = {k : v if type(v) is not np.bytes_ else str(v) for k, v in dataFile.attrs.items()}
            data[tag]["pos"] = dataFile["positions"][()].tolist()
    storeJSON(outName, data)
except Exception as e:
    print("Node placement data extraction failed with:")
    print(e)

# %% Refinement counts
outName = "plotData/refinementScheme.json.gz"
path = r"../code/data/sizes"
file_index = {}
for file in os.listdir(path):
    if file.endswith(".h5"):
        try:
            with h5py.File(os.path.join(path, file), "r") as dataFile:
                file_index[file] = {"h" : dataFile.attrs["num.h"],
                                    "variable_density" : dataFile.attrs["fill.variable_density"],
                                    "N" : len(dataFile["positions"][0])}
                if dataFile.attrs["fill.variable_density"]:
                    file_index[file]["dense_band"] = dataFile.attrs["fill.dense_band"]
                    file_index[file]["max_step"] = dataFile.attrs["fill.max_step"]
                    file_index[file]["refine_ratio"] = dataFile.attrs["fill.refine_ratio"]
                    file_index[file]["pos"] = dataFile["positions"][()].tolist()
        except (KeyError, OSError, RuntimeError) as e:
            print(f"Reading {file} failed with {e}")
storeJSON(outName, file_index)

# %% Timestep evolution
outName = "plotData/timestepEvolution.json.gz"
path = r"../code/data/dtEvolution"
data = {}
for file in os.listdir(path):
    if file.endswith(".h5"):
        try:
            with h5py.File(os.path.join(path, file), "r") as dataFile:
                steps = sorted(list(dataFile["/values"].keys()), key=lambda x: int(x.replace("step", "")))
                attrs = dict(dataFile.attrs)
                timeFactor = attrs["phy.lam"] / attrs["phy.c_p"] / attrs["phy.rho"] / attrs["case.height"]**2
                d = data.setdefault(float(attrs["num.h"]), {}).setdefault(float(attrs["dimensionless.Ra"]), {})
                d[float(attrs["phy.power_index"])] = {"h" : attrs["num.h"],
                                               "N" : len(dataFile["positions"][0]),
                                               "attrs" : {k : v if type(v) is not np.bytes_ else str(v) for k, v in attrs.items()},
                                               "nusselt" : [dataFile[f"/values/{s}"].attrs["nusselt"] for s in steps],
                                               "dt" : [dataFile[f"/values/{s}"].attrs["dt"] * timeFactor for s in steps],
                                               "t" : [dataFile[f"/values/{s}"].attrs["time"] * timeFactor for s in steps]}
        except (KeyError, OSError, RuntimeError) as e:
            print(f"Reading {file} failed with {e}")
storeJSON(outName, data)

# %% Refinement parameter analysis
path = "../code/data/refinement_convergence_acm/"
outName = "plotData/refineParameterConvergence.json.gz"

folders = {"ratio" : "ratio", "band" : "stepWthRatio"}

data = {}
for case, folder in folders.items():
    for file in os.listdir(os.path.join(path, folder)):
        if file.endswith(".h5"):
            try:
                with h5py.File(os.path.join(path, folder, file), "r") as dataFile:
                    steps = sorted(list(dataFile["/values"].keys()), key=lambda x: int(x.replace("step", "")))
                    endTime = dataFile[f"/values/{steps[-1]}"].attrs["time"]
                    if endTime < 0.9 * dataFile.attrs["case.end_time"]:
                        continue
                    if case not in data:
                        data[case] = {}
                    data[case][file] = {"attrs" : {k : v if type(v) is not np.bytes_ else str(v) for k, v in dataFile.attrs.items()},
                                        "h" : dataFile.attrs["num.h"],
                                        "refine_ratio" : dataFile.attrs["fill.refine_ratio"] if "fill.refine_ratio" in dataFile.attrs else dataFile.attrs["fill.max_step"] / dataFile.attrs["num.h"],
                                        "dense_band" : dataFile.attrs["fill.dense_band"],
                                        "nusselt" : dataFile[f"/values/{steps[-1]}"].attrs["nusselt"],
                                        "maxDiv" : np.max(np.abs(dataFile["divergence"])),
                                        "N" : len(dataFile["positions"][0])}
            except (KeyError, OSError, RuntimeError) as e:
                print(f"Reading {file} failed with {e}")
storeJSON(outName, data)

# %% refined convergence
path = "../code/data/refinement/"
out = "plotData/refinedConvergence.json.gz"
try:
    dirToJSON(path, out)
except Exception as e:
    print("Refined convergence data extraction failed with:")
    print(e)

# %% convergence
path = "../code/data/convergence/"
out = "plotData/monOrderConvergence.json.gz"
try:
    dirToJSON(path, out)
except Exception as e:
    print("Convergence data extraction failed with:")
    print(e)


