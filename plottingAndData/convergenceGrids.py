# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate as interpolate
import h5py
import os
import json
import gzip

plt.rc('text', usetex = False)
#plt.rc('font', size = 14, family = 'serif', serif = ['Arial'])
plt.rc('font', size = 14, family = 'serif', serif = ['STIXGeneral'])
plt.rc('xtick', labelsize = 'small')
plt.rc('ytick', labelsize = 'small')
plt.rc('legend', frameon = True, fontsize = 'medium', loc = "best")
plt.rc('figure', figsize = (10, 6), facecolor = "white")
plt.rc('lines', linewidth=1.0)
plt.rc('axes', xmargin = 0, ymargin = 0, autolimit_mode = 'round_numbers')
plt.rc('mathtext', fontset = 'cm', rm = 'serif')

plt.close('all')

def toScientific(number, decimals = 1):
    try:
        if number == np.inf:
            return "$\infty$"
        exponent = int(np.floor(np.log10(np.abs(number))))
        if decimals < 0:
            return f"$10^{ {exponent} }$"
        elif exponent == 0:
            return str(np.round(number, decimals))
        elif exponent == 1:
            return str(np.round(number, decimals))
        elif exponent == 2:
            return str(np.round(number, decimals))
        elif exponent == -1:
            return str(np.round(number, decimals+1))
        elif exponent == -2:
            return str(np.round(number, decimals+2))
        else:
            return f"${np.round(number / np.power(10.0, exponent), decimals)} \cdot 10^{ {exponent} }$"
    except Exception as e:
        print("Got exception: ", e)
        return "NaN"

#------------------------------------------------------------------------------
with gzip.open("plotData/monOrderConvergence.json.gz", 'r') as zipfile:
    data = json.load(zipfile)

# %% Nusselt convergence grid
rayleighs = [10000, 100000, 1000000]
power_indices = [0.6, 0.7, 0.8, 0.9, 1]

fig, ax = plt.subplots(len(power_indices), len(rayleighs), sharex=True, figsize=(10, 8))
rowIdx = -1

for n in power_indices:
    colIdx = -1
    rowIdx += 1
    for Ra in rayleighs:
        colIdx += 1
        ax[0, colIdx].set_title(f"Ra$=${toScientific(Ra, -1)}")
        colors = iter(plt.rcParams['axes.prop_cycle'].by_key()['color'])
        colorMapping = {}
        plot_data = {}
        for filename, contents in data.items():
            attrs = contents["attrs"]
            if attrs["phy.power_index"] == n and attrs["dimensionless.Ra"] == Ra:
                nusselts = contents["Nu"]
                N = contents["N"]
                
                d = plot_data.setdefault(attrs["num.mon_order"], [])
                d.append([N, nusselts[-1]])
                    
        for key, values in sorted(plot_data.items(), key=lambda x: x[0]):
            if Ra == 1000000 and n == 0.6:
                print(np.array(sorted(values, key=lambda x: x[0])).T)
            ax[rowIdx, colIdx].plot(*np.array(sorted(values, key=lambda x: x[0])).T, '.--', label=f"$m={key:.0f}$")
        ax[rowIdx, colIdx].grid(linestyle="-")
        ax[rowIdx, colIdx].set_xscale("log")
    ax[rowIdx, 0].set_ylabel( f"$n = {n}$" + "\n" + r"$\overline{\mathrm{Nu}}$")
    
# handles, labels = ax[0, 0].get_legend_handles_labels()
# ax[0, 0].legend(handles[:2], labels[:2])
# ax[0, 1].legend(handles[2:], labels[2:])
ax[0, 0].legend()
for col in ax[-1]:
    col.set_xlabel("$N$")
    col.set_xlim(0, 220000)
    
fig.tight_layout()

# fig.savefig("../figures/nusseltConvergence.pdf")
# fig.savefig("../figures/nusseltConvergence", dpi=300)

# %% symmetry error convergence grid
rayleighs = [10000, 100000, 1000000]
power_indices = [0.6, 0.7, 0.8, 0.9, 1]

cross_location = 0.75
coordinates = [cross_location, 1 - cross_location]
interp_count = 1000

fig, ax = plt.subplots(len(power_indices), len(rayleighs), sharey=True, sharex=True, figsize=(10, 8))
rowIdx = -1

for n in power_indices:
    colIdx = -1
    rowIdx += 1
    for Ra in rayleighs:
        colIdx += 1
        ax[0, colIdx].set_title(f"Ra$=${toScientific(Ra, -1)}")
        colors = iter(plt.rcParams['axes.prop_cycle'].by_key()['color'])
        colorMapping = {}
        plot_data = {}
        for filename, contents in data.items():
            attrs = contents["attrs"]
            if attrs["phy.power_index"] == n and attrs["dimensionless.Ra"] == Ra:
                h = attrs["num.h"]
                nusselts = contents["Nu"]
                N = contents["N"]
                pos = np.array(contents["pos"])
                vy = np.array(contents["v"][1])
                vy = vy * attrs["phy.c_p"] * attrs["phy.rho"] * attrs["case.height"] / attrs["phy.lam"]
                
                interpolated = []
                for idx in range(len(coordinates)):
                    interp_loc = np.array([np.linspace(0, 1, interp_count), np.ones(interp_count) * coordinates[idx]]).T
                    relevant_indices = ((pos[1] > coordinates[idx] - 5 * h) * (pos[1] < coordinates[idx] + 5 * h))
                    interpolated.append(interpolate.griddata(pos[:, relevant_indices].T, vy[relevant_indices], interp_loc,
                                        method="cubic"))


                    if idx != 0: # Invert the second velocity profile for direct comparison
                        interpolated[idx] *= -1
                        interpolated[idx] = interpolated[idx][::-1]
                
                symErr = max(np.abs(interpolated[1] - interpolated[0])) / np.max(np.abs([interpolated[1], interpolated[0]]))
                
                d = plot_data.setdefault(attrs["num.mon_order"], [])
                d.append([N, symErr])
        
        for key, values in sorted(plot_data.items(), key=lambda x: x[0]):
            ax[rowIdx, colIdx].plot(*np.array(sorted(values, key=lambda x: x[0])).T, '.--', label=f"$m={key:.0f}$")
        ax[rowIdx, colIdx].set_yscale("log")
        ax[rowIdx, colIdx].set_xscale("log")
        ax[rowIdx, colIdx].set_ylim(1e-5, 1)
        ax[rowIdx, colIdx].set_yticks([1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1e0])
        ax[rowIdx, colIdx].grid(linestyle="-")
    # ax[rowIdx, 0].set_ylabel( f"$n = {n}$" + "\n" + r"$v_y$ symmetry error")
    ax[rowIdx, 0].set_ylabel( f"$n = {n}$" + "\n" + r"$\epsilon$")
    
# handles, labels = ax[0, 0].get_legend_handles_labels()
# ax[-1, -2].legend(handles[:2], labels[:2])
# ax[-1, -1].legend(handles[2:], labels[2:])
ax[0, -1].legend()
for col in ax[-1]:
    col.set_xlabel("$N$")
    col.set_xlim(0, 220000)
    
fig.tight_layout()

# fig.savefig("../figures/symmetryErrorGrid.pdf")
# fig.savefig("../figures/symmetryErrorGrid", dpi=300)

# %% Convergence rate
rayleighs = [10000, 100000, 1000000]
power_indices = [0.6, 0.7, 0.8, 0.9, 1]

fig, ax = plt.subplots(len(power_indices), len(rayleighs), sharex=True, figsize=(10, 8))
rowIdx = -1
lsp = np.linspace(3e-2, 1e-3, 3)
for n in power_indices:
    colIdx = -1
    rowIdx += 1
    for Ra in rayleighs:
        colIdx += 1
        ax[0, colIdx].set_title(f"Ra$=${toScientific(Ra, -1)}")
        colors = iter(plt.rcParams['axes.prop_cycle'].by_key()['color'])
        colorMapping = {}
        plot_data = {}
        for filename, contents in data.items():
            attrs = contents["attrs"]
            if attrs["phy.power_index"] == n and attrs["dimensionless.Ra"] == Ra:
                nusselts = contents["Nu"]
                N = contents["N"]
                
                d = plot_data.setdefault(attrs["num.mon_order"], [])
                d.append([attrs["num.h"], nusselts[-1]])
        
        for key, values in sorted(plot_data.items(), key=lambda x: x[0]):
            values = sorted(values, key=lambda x: 1/x[0])
            arrVal = np.array(values[:-1]).T
            arrVal[1] = np.abs(arrVal[1] - values[-1][1])
            ax[rowIdx, colIdx].plot(*arrVal, '.--', label=f"$m={key:.0f}$")
        ax[rowIdx, colIdx].plot(lsp, np.power(10 * lsp, 2), 'k:', alpha=0.8, label="$\mathcal{O}(h^2)$")
        ax[rowIdx, colIdx].grid(linestyle="-")
        ax[rowIdx, colIdx].set_xscale("log")
        ax[rowIdx, colIdx].set_yscale("log")
        ax[rowIdx, colIdx].xaxis.set_inverted(True)
    ax[rowIdx, 0].set_ylabel( f"$n = {n}$" + "\n" + r"$\overline{\mathrm{Nu}}$")
    
# handles, labels = ax[0, 0].get_legend_handles_labels()
# ax[0, 0].legend(handles[:2], labels[:2])
# ax[0, 1].legend(handles[2:], labels[2:])
ax[-1, 0].legend()
for col in ax[-1]:
    col.set_xlabel("$h$")
    col.set_xlim(3e-2, 1e-3)
    
fig.tight_layout()

# fig.savefig("../figures/nusseltConvergenceRate.pdf")
# fig.savefig("../figures/nusseltConvergenceRate", dpi=300)