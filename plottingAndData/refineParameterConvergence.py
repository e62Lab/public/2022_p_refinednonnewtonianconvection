# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import matplotlib.tri as triang
import numpy as np
import h5py
import os
import json
import scipy.interpolate as interpolate
import cmocean
import gzip

import nusseltFits

plt.rc('text', usetex = False)
#plt.rc('font', size = 14, family = 'serif', serif = ['Arial'])
plt.rc('font', size = 14, family = 'serif', serif = ['STIXGeneral'])
plt.rc('xtick', labelsize = 'small')
plt.rc('ytick', labelsize = 'small')
plt.rc('legend', frameon = True, fontsize = 'medium', loc = "best")
plt.rc('figure', figsize = (10, 6), facecolor = "white")
plt.rc('lines', linewidth=1.0)
plt.rc('axes', xmargin = 0, ymargin = 0, autolimit_mode = 'round_numbers')
plt.rc('mathtext', fontset = 'cm', rm = 'serif')

def toScientific(number, decimals = 1):
    try:
        if number == np.inf:
            return "$\infty$"
        exponent = int(np.floor(np.log10(np.abs(number))))
        if decimals < 0:
            return f"$10^{ {exponent} }$"
        elif exponent == 0:
            return str(np.round(number, decimals))
        elif exponent == 1:
            return str(np.round(number, decimals))
        elif exponent == 2:
            return str(np.round(number, decimals))
        elif exponent == -1:
            return str(np.round(number, decimals+1))
        elif exponent == -2:
            return str(np.round(number, decimals+2))
        else:
            return f"${np.round(number / np.power(10.0, exponent), decimals)} \cdot 10^{ {exponent} }$"
    except Exception as e:
        print("Got exception: ", e)
        return "NaN"

plt.close('all')
#------------------------------------------------------------------------------
jsonPath = "plotData/refineParameterConvergence.json.gz"
with gzip.open(jsonPath, 'r') as zipfile:
    data = json.load(zipfile)

# %% Refinement parameter convergence analysis

boundaryDiscrt = []
for name, contents in data["ratio"].items():
    if contents["h"] not in boundaryDiscrt:
        boundaryDiscrt.append(contents["h"])
boundaryDiscrt = sorted(boundaryDiscrt)

fig, ax = plt.subplots(2, 2, sharex="col", figsize=(10, 5.5))
for h in boundaryDiscrt:
    # Refinement ratio plot
    x, y1, y2 = [], [], []
    for name, contents in data["ratio"].items():
        if contents["h"] == h:
            x.append(contents["refine_ratio"])
            y1.append(contents["nusselt"])
            y2.append(contents["N"])
    order = np.argsort(x)
    ax[0, 0].plot([x[i] for i in order], [y1[i] / y1[order[0]] for i in order], '.--', label=f"$h_1={h}$")
    ax[1, 0].plot([x[i] for i in order], [y2[i] for i in order], '.--', label=f"$h_1={h}$")
    # ax[1, 0].plot(0.05/h, y2[order[-1]], 'k.')
    # Unrefined band width plot
    x, y1, y2 = [], [], []
    for name, contents in data["band"].items():
        if contents["h"] == h:
            x.append(contents["dense_band"])
            y1.append(contents["nusselt"])
            y2.append(contents["N"])
    order = np.argsort(x)
    ax[0, 1].plot([x[i] for i in order], [y1[i] / y1[order[-1]] for i in order], '.--', label=f"$h_1={h}$")
    ax[1, 1].plot([x[i] for i in order], [y2[i] for i in order], '.--', label=f"$h_1={h}$")
ax[1, 0].legend()

ax[1, 0].set_xlabel(r"$\frac{h_2}{h_1}$")
ax[1, 1].set_xlabel(r"$w$")
ax[0, 0].set_ylabel(r"$\frac{\overline{\mathrm{Nu}}}{\overline{\mathrm{Nu}}_1}$")
ax[0, 1].set_ylabel(r"$\frac{\overline{\mathrm{Nu}}}{\overline{\mathrm{Nu}}_{0.1}}$")
ax[1, 0].set_ylabel(r"$N$")

for aa in ax:
    for a in aa:
        a.grid(linestyle="-")

fig.tight_layout()
# fig.savefig("../../figures/refineParameterConvergence.pdf")
# fig.savefig("../../figures/refineParameterConvergence", dpi=300)