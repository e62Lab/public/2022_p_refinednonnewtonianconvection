#ifndef NONNEWTONIANTHERMOFLUIDREFINEMENT_CALCULATIONENVIRONMESTBASE_HPP
#define NONNEWTONIANTHERMOFLUIDREFINEMENT_CALCULATIONENVIRONMESTBASE_HPP

#include <medusa/Medusa.hpp>

class CalculationEnvironmentBase {
protected:
    double time_step_, time_, start_time_, end_time_;
    int output_count_ = -1;
    double last_output_time_;
    bool stepping_initialized_ = false;
    double printout_interval_;
    bool printout_initialized_ = false;
    std::chrono::steady_clock::time_point last_printout_;
    std::chrono::steady_clock::time_point start_timestamp_;

    std::string time_unit = "min";
    double time_unit_factor = 60;

public:
    void initializeTimeStepping(double time_step, double end_time, double start_time=0) {
        time_step_ = time_step;
        time_ = start_time;
        start_time_ = start_time;
        end_time_ = end_time;
        stepping_initialized_ = true;
    }

    void initializeStorage(int output_count) {
        output_count_ = output_count;
    }

    void initializePrintout(double printout_interval) {
        printout_interval_ = printout_interval;
        printout_initialized_ = true;
    }

//TODO: too much for one method, refactor
    bool run() {
        initialize();
        if (!stepping_initialized_) {
            std::cout << "Time stepping has not been initialized, "
                      << "please call initializeTimeStepping(...)" << std::endl;
            return false;
        }
        storeInitial();

        last_output_time_ = start_time_;
        double intermediate_interval = (output_count_ < 1)
                                    ? end_time_ - start_time_ + 1 // Never
                                    : (end_time_ - start_time_) / output_count_;
        if (printout_initialized_) {
            start_timestamp_ = std::chrono::steady_clock::now();
        }
        int step = 0;
        while (time_ < end_time_) {
            if (time_ - last_output_time_ > intermediate_interval) {
                if (terminateAtStoreIntermediate()) {
                    return false;
                }
                storeIntermediate(step);
                last_output_time_ = time_;
            }

            if (printout_initialized_) {
                auto current_time = std::chrono::steady_clock::now();
                if (std::chrono::duration<double>(current_time - last_printout_).count()
                                                                    > printout_interval_
                    || time_ == start_time_) {
                    std::cout.setf(std::ios::fixed, std::ios::floatfield);
                    std::cout << progress(current_time) << "|" << printout() << std::endl;
                    last_printout_ = current_time;
                }
            }

            timeStep();
            time_ += time_step_;
            ++step;
        }
        storeFinal(step);
        return true;
    }

protected:
    // set up everything necessary for calculations in timeStep()
    // called at the start of run() method, other initializers should be called within or before
    virtual void initialize() = 0;

    // core time stepping method
    virtual void timeStep() = 0;

    virtual void storeInitial() {
        // re-implement in inherited if necessary
    };

    virtual void storeIntermediate(int step) {
        // re-implement in inherited if necessary
    };

    virtual void storeFinal(int step) {
        // re-implement in inherited if necessary
    };

    virtual bool terminateAtStoreIntermediate() {
        // re-implement in inherited if necessary
        return false;
    };
//TODO: implement initial printout
    virtual std::string printout() {
        // re-implement in inherited if necessary
        return "Printout function has not been re-implemented.";
    };

    std::string progress(std::chrono::steady_clock::time_point& current_time) {
        double time_elapsed = std::chrono::duration<double>(current_time - start_timestamp_).count();
        double time_estimated = (time_ == start_time_)
                                    ? std::numeric_limits<double>::quiet_NaN()
                                    : time_elapsed * (end_time_ - time_) / (time_ - start_time_);
        std::stringstream ss;
        ss << std::fixed << std::setprecision(1) << 100 * (time_ - start_time_) / (end_time_ - start_time_)
           << std::setprecision(0) << "%|" << time_elapsed / time_unit_factor << "-"
           << time_estimated / time_unit_factor << "[" << time_unit << "]";
        return ss.str();
    }
};

#endif //NONNEWTONIANTHERMOFLUIDREFINEMENT_CALCULATIONENVIRONMESTBASE_HPP
