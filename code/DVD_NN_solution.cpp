#include <medusa/Medusa.hpp>
#include "DVD.hpp"

using namespace mm;

#define EPS 1e-10 //small number


constexpr int dim = 2;
constexpr int phs_order = 3;

int main() {
    const std::string parameter_file = "../params/params.xml";

    // delete old data file instance
    {
        XML params(parameter_file);
        HDF hdf_file(params.get<std::string>("output.hdf5_name"), HDF::DESTROY);
        omp_set_num_threads(params.get<int>("sys.num_threads"));
    }

    XML params(parameter_file);
    DVD<dim, phs_order> solution(params);
    solution.run();
}

