#include <medusa/Medusa.hpp>
#include "PorousCavity.hpp"

using namespace mm;

constexpr int phs_order = 3;

int main(int arg_num, char* arg[]) {
    std::string parameter_file(arg[1]);
    std::string output_name = mm::join({mm::split(mm::split(parameter_file, "/").back(),".").front(), "h5"}, ".");

    XML params(parameter_file);
    params.set("output.hdf5_name", output_name, true);
    if (params.get<bool>("output.clear_hdf5")) {
        HDF hdf_file(params.get<std::string>("output.hdf5_name"), HDF::DESTROY);
    }
    omp_set_num_threads(params.get<int>("sys.num_threads"));

    int dim = params.get<int>("case.dim");
    if (dim == 2) {
        PorousCavity<2, phs_order> solution(params);
        solution.run();
    } else if (dim == 3) {
        PorousCavity<3, phs_order> solution(params);
        solution.run();
    } else {
        std::cout << "dim=" << dim << " is not supported.";
    }
}

