#ifndef STABILISEDNONNEWTONIAN_NONNEWTONIANCONVECTION_HPP
#define STABILISEDNONNEWTONIAN_NONNEWTONIANCONVECTION_HPP

#include <medusa/Medusa.hpp>
#include <iostream>
#include <Eigen/SparseCore>
#include <Eigen/SparseLU>
#include <medusa/IO.hpp>
#include <mutex>

#include "CalculationEnvironmentBase.hpp"

using namespace mm;

#define EPS 1e-10 //small number

template <int dim, int phs_order>
class NonNewtonianConvection: public CalculationEnvironmentBase {
protected:
    Timer tim_;
    XML &xml_in_;
    HDF hdf_out_;

    typedef double scal_t;
    typedef Vec<scal_t, dim> vec_t;
    typedef std::tuple<Der1s<dim>, Lap<dim>> operator_t;
    typedef RaggedShapeStorage<vec_t, operator_t> storage_t;

    scal_t h_, dt_, c_p_, rho_, lam_, mu_, power_index_, alpha_,
            beta_, T_ref_, T_cold_, T_hot_, Ra_, Pr_, courant_1_, courant_2_;
    Vec<scal_t, dim> g_;
    int stp_ = 0;

    DomainDiscretization<vec_t> domain_;
    storage_t storage_;
    ExplicitOperators<storage_t> op_scalar_;
    ExplicitVectorOperators<storage_t> op_vector_;

    ScalarFieldd T_1_, T_2_, viscosity_;
    VectorField<scal_t, dim> u_1_, u_2_;
    Range<Eigen::Matrix<scal_t, dim, dim>> vel_grad_;
    Range<VectorField<scal_t, dim>> viscous_stress_, viscous_stress_smooth_;
    Range<int> interior_;
    Range<Range<int>> edge_indices_;
    enum Edge : int {HOT, COLD, NEUTRAL, type_count};

    Range<scal_t> times_;

    Eigen::VectorXd rhs_;
    Eigen::SparseLU<Eigen::SparseMatrix<scal_t>> solver_;


public:
    explicit NonNewtonianConvection(XML &xml_params)
            : xml_in_{xml_params}, domain_(UnknownShape<vec_t>()) {
        HDF::Mode mode = xml_in_.get<bool>("output.clear_hdf5")
                         ? HDF::DESTROY
                         : HDF::APPEND;
        hdf_out_.open(xml_in_.get<std::string>("output.hdf5_name"), "/", mode);
    }

    ~NonNewtonianConvection() {
        hdf_out_.close();
    }

    void initialize() override {
        tim_.addCheckPoint("init");

        omp_set_num_threads(xml_in_.get<int>("sys.num_threads"));

        loadVariables();

        initializeTimeStepping(dt_, xml_in_.get<scal_t>("case.end_time"));
        initializeStorage(xml_in_.get<int>("output.count"));
        initializePrintout(xml_in_.get<int>("output.printout_interval"));

        constructDomain(xml_in_.get<scal_t>("case.height"));
        constructApproximation();
        setInitialValues();

        std::cout << "h=" << h_ << ", dt=" << dt_ << ", N=" << domain_.size() << std::endl;

        tim_.addCheckPoint("start iteration");
    }

    void loadVariables() {
        c_p_ = xml_in_.get<scal_t>("phy.c_p");
        rho_ = xml_in_.get<scal_t>("phy.rho");
        lam_ = xml_in_.get<scal_t>("phy.lam");
        mu_ = xml_in_.get<scal_t>("phy.mu");
        power_index_ = xml_in_.get<scal_t>("phy.power_index");
        beta_ = xml_in_.get<scal_t>("phy.beta");
        T_ref_ = xml_in_.get<scal_t>("phy.T_ref");
        T_hot_ = xml_in_.get<scal_t>("case.T_hot");
        T_cold_ = xml_in_.get<scal_t>("case.T_cold");
        h_ = xml_in_.get<scal_t>("num.h");
        auto g_0 = xml_in_.get<scal_t>("phy.g_0");

        alpha_ = lam_ / c_p_ / rho_;

        auto rayleigh = xml_in_.get<scal_t>("dimensionless.Ra");
        auto prandtl = xml_in_.get<scal_t>("dimensionless.Pr");
        auto height = xml_in_.get<scal_t>("case.height");
        if (xml_in_.get<int>("dimensionless.forceDimensionless")) {
            mu_ = prandtl * std::pow(alpha_, 2 - power_index_)
                  * rho_ * std::pow(height, power_index_ - 1);
            std::cout << "Prandtl number " << prandtl << " enforced by changing mu to " << mu_ << std::endl;
            xml_in_.set("phy.mu", mu_, true);
            beta_ = rayleigh * mu_ * std::pow(alpha_, power_index_)
                    * std::pow(height, -2 - power_index_) / rho_ / std::abs(g_0 *(T_hot_ - T_cold_));
            std::cout << "Rayleigh number " << rayleigh << " enforced by changing beta to " << beta_ << std::endl;
            xml_in_.set("phy.beta", beta_, true);
        }

        Ra_ = rho_ * beta_ * std::abs(g_0 *(T_hot_ - T_cold_)) * std::pow(height, power_index_ + 2) / std::pow(alpha_, power_index_) / mu_;
        Pr_ = mu_ * std::pow(alpha_, power_index_ - 2) * std::pow(height, 1 - power_index_) / rho_;

        dt_ = xml_in_.get<scal_t>("num.dt");
        if (xml_in_.exists("num.variable_dt") && xml_in_.get<bool>("num.variable_dt")) {
            courant_1_ = xml_in_.get<scal_t>("num.courant_1");
            courant_2_ = xml_in_.get<scal_t>("num.courant_2");
        } else {
            courant_1_ = -1;
            courant_2_ = -1;
        }

        g_.setZero();
        g_[dim - 1] = g_0; // gravity is defined to work in the last dimension
    }

    virtual void constructDomain(scal_t height) {}

    void constructApproximation() {
        tim_.addCheckPoint("approximation");
        Polyharmonic<scal_t, phs_order> phs;
        Monomials<vec_t> mon(xml_in_.get<scal_t>("num.mon_order"));
        int support_size = std::round(xml_in_.get<scal_t>("num.support_size_factor") * mon.size()) + 1;
        domain_.findSupport(FindClosest(support_size).forNodes(domain_.interior()));
        domain_.findSupport(FindClosest(support_size).forNodes(domain_.boundary()).searchAmong(domain_.interior()).forceSelf(true));
        RBFFD<decltype(phs), vec_t, ScaleToClosest> appr(phs, mon);

        std::cout << "Explicit momentum and heat equations with implicit pressure projection" << std::endl;
        std::cout << "Domain size=" << domain_.size() << ", Support size=" << support_size << std::endl;

        operator_t ops;
        storage_.resize(domain_.supportSizes());
        computeShapes(domain_, appr, domain_.all(), ops, &storage_);

        op_scalar_ = storage_.explicitOperators();
        op_vector_ = storage_.explicitVectorOperators();

        int N = domain_.size();

        Eigen::SparseMatrix<scal_t, Eigen::RowMajor> M(N + 1, N + 1);
        rhs_.resize(N + 1);
        rhs_.setZero();
        auto op_implicit = storage_.implicitOperators(M, rhs_);

        // pressure correction
        for (int i : domain_.interior()) {
            op_implicit.lap(i).eval(1);
        }
        for (int i : domain_.boundary()) {
            op_implicit.neumann(i, domain_.normal(i)).eval(1);
        }
        // regularization - set the last row and column of the matrix
        for (int i = 0; i < N; ++i) {
            M.coeffRef(N, i) = 1;
            M.coeffRef(i, N) = 1;
        }
        // set the sum of all values
        rhs_[N] = 0.0;
        M.makeCompressed();
        solver_.compute(M);
        if (solver_.info() != Eigen::Success) {
            std::cout << "LU factorization failed with error:"
                      << solver_.lastErrorMessage() << std::endl;
        }
    }

    virtual void setInitialValues() {
        auto T_cold = xml_in_.template get<scal_t>("case.T_cold");
        auto T_hot = xml_in_.template get<scal_t>("case.T_hot");
        T_1_.resize(domain_.size());
        T_1_ = xml_in_.template get<scal_t>("case.T_init");
        auto borders = domain_.shape().bbox();
        scal_t length = borders.second[0] - borders.first[0];
        T_1_[edge_indices_[COLD]] = T_cold;
        T_1_[edge_indices_[HOT]] = T_hot;
        u_1_.resize(domain_.size(), dim);
        u_1_.setZero();
        if (xml_in_.exists("case.load_initial")
            && xml_in_.template get<scal_t>("case.load_initial")) {
            loadInitialValues();
        }
        T_2_ = T_1_;
        u_2_ = u_1_;

        viscous_stress_.resize(dim);
        viscous_stress_smooth_.resize(dim);
        for (int i = 0; i < dim; ++i) {
            viscous_stress_[i].resize(domain_.size(), dim);
            viscous_stress_[i].setZero();
            viscous_stress_smooth_[i].resize(domain_.size(), dim);
            viscous_stress_smooth_[i].setZero();
        }

        viscosity_.resize(domain_.size());
        vel_grad_.resize(domain_.size());
    }

    void loadInitialValues() {
        int neighbours = 10; // TODO: set as param if neccessary
        auto load_file = xml_in_.template get<std::string>("case.load_from");
        HDF hdf_in(load_file, HDF::READONLY);
        auto positions = hdf_in.readDouble2DArray("positions");
        auto velocities = hdf_in.readEigen("velocities");
        auto temperatures = hdf_in.readEigen("temperatures");
        Range<vec_t> pos;
        Range<scal_t> v_x, v_y, T;
        for (unsigned i = 0; i < positions.size(); ++i) {
            pos.push_back(vec_t::Zero());
            for (int j = 0; j < dim; ++j) {
                pos.back()[j] = positions[i][j]; // TODO: Implement a more general solution.
            }
            v_x.push_back(velocities(i, 0));
            v_y.push_back(velocities(i, 1));
            T.push_back(temperatures(i));
        }
        SheppardInterpolant<vec_t, scal_t> intp_v_x(pos, v_x);
        SheppardInterpolant<vec_t, scal_t> intp_v_y(pos, v_y);
        SheppardInterpolant<vec_t, scal_t> intp_T(pos, T);

        #pragma omp parallel for default(none) shared(intp_v_x, intp_v_y, intp_T, neighbours)
        for (int intIdx = 0; intIdx < interior_.size(); ++intIdx) {
            int i = interior_[intIdx];
            auto p = domain_.pos(i);
            T_1_(i) = intp_T(p, neighbours);
            u_1_(i, 0) = intp_v_x(p, neighbours);
            u_1_(i, 1) = intp_v_y(p, neighbours);
        }
        #pragma omp parallel for default(none) shared(intp_v_x, intp_v_y, intp_T, neighbours)
        for (int intIdx = 0; intIdx < edge_indices_[NEUTRAL].size(); ++intIdx) {
            int i = edge_indices_[NEUTRAL][intIdx];
            auto p = domain_.pos(i);
            T_1_(i) = intp_T(p, neighbours);
        }

        T_2_ = T_1_;
        u_2_ = u_1_;
    }

    void timeStep() override {
        scal_t max_norm = 0;
        scal_t dt_1 = 1e-4, dt_2 = 1e-4; //TODO: Move max timestep to params
        #pragma omp parallel for default(none) schedule(static) \
                reduction(max : max_norm) reduction(min : dt_1, dt_2)
        for (int i = 0; i < domain_.size(); ++i) {
            max_norm = std::max(u_1_[i].norm(), max_norm);
            vel_grad_[i] = op_vector_.grad(u_1_, i);
            scal_t norm = (vel_grad_[i] + vel_grad_[i].transpose()).squaredNorm() / 2;
            norm = std::max(norm, EPS);
            viscosity_[i] = mu_ * std::pow(norm, (power_index_ - 1) / 2);

            if (courant_1_ > 0) {
                scal_t dx = (domain_.supportNode(i, 1) - domain_.supportNode(i, 0)).norm();
                dt_1 = std::min(dt_1, courant_1_ * dx / u_1_[i].norm());
                dt_2 = std::min(dt_2, courant_2_ * dx * dx * rho_ / viscosity_[i] / 2);
            }
        }

        if (courant_1_ > 0) {
            dt_ = std::min(dt_1, dt_2);
            this->time_step_ = dt_;
        }

        #pragma omp parallel for default(none) schedule(static)
        for (int i = 0; i < interior_.size(); ++i) {
            int c = interior_[i];
            u_2_[c] = u_1_[c] + dt_ * (
                                        ((vel_grad_[c] + vel_grad_[c].transpose()) * op_scalar_.grad(viscosity_, c)
                                        + viscosity_[c] * op_vector_.lap(u_1_, c)) / rho_
                                       - vel_grad_[c] * u_1_[c]
                                       + g_ * (-beta_ * (T_1_[c] - T_ref_))
            );
        }


        // pressure correction
        #pragma omp parallel for default(none) schedule(static)
        for (int i = 0; i < interior_.size(); ++i) {
            int c = interior_[i];
            rhs_(c) = rho_ / dt_ * op_vector_.div(u_2_, c);
        }
        for (int i = 0; i < edge_indices_.size(); ++i) {
            for (int j : edge_indices_[i]) rhs_(j) = rho_ / dt_ * u_2_[j].dot(domain_.normal(j));
        }

        ScalarFieldd p = solver_.solve(rhs_).head(domain_.size());

        #pragma omp parallel for default(none) shared(p) schedule(static)
        for (int i = 0; i < interior_.size(); ++i) {
            int c = interior_[i];
            u_2_[c] -= dt_ / rho_ * op_scalar_.grad(p, c);
        }

        scal_t max_T = std::max(T_hot_, T_cold_);
        scal_t min_T = std::min(T_hot_, T_cold_);

        // explicit heat transfer
        #pragma omp parallel for default(none) shared(min_T, max_T) schedule(static)
        for (int i = 0; i < interior_.size(); ++i) {
            int c = interior_[i];
            T_2_[c] = std::max(std::min(T_1_[c] + dt_ * alpha_ * op_scalar_.lap(T_1_, c)
                                        - dt_ * u_2_[c].transpose() * op_scalar_.grad(T_1_, c)
                    , max_T),
                               min_T);
        }

        #pragma omp parallel for default(none) shared(min_T, max_T) schedule(static)
        for (int i = 0; i < edge_indices_[NEUTRAL].size(); ++i) {
            int c = edge_indices_[NEUTRAL][i]; // top edge
            T_2_[c] = std::max(std::min(op_scalar_.neumann(T_2_, c, domain_.normal(c), 0.0),
                                        max_T),
                               min_T);
        }

        T_1_.swap(T_2_);
        u_1_.swap(u_2_);
    }

    virtual bool terminateAtStoreIntermediate() {
        for ( int i = 0; i < u_2_.rows(); ++i) {
            for (int j = 0; j < dim; ++j) {
                if (std::isnan(u_2_.coeff(i, j))) {
                    std::cout << "nan node " << i << std::endl;
                    return true;
                }
            }
        }
        return false;
    };

    std::string printout() override {
        scal_t max_div = 0;
        for (int i : interior_) {
            max_div = std::max(max_div, op_vector_.div(u_2_, i));
        }
        std::stringstream ss;
        ss << std::setprecision(5) << "T: min=" << T_2_.minCoeff() << " max=" << T_2_.maxCoeff()
           << " avg=" << T_2_.mean() << ", mean u:" << u_2_.mean() << ", max div: " << max_div;
        return ss.str();
    }

    ScalarField<scal_t> calculateNusselt(indexes_t where) {
        // TODO: add L (not needed as char L = 1, reconsider)
        ScalarField<scal_t> nusselt(where.size());
        int idx = 0;
        for (auto i : where) {
            nusselt(idx++) = std::abs(op_scalar_.d1(T_2_, 0, i));
        }
        nusselt /= std::abs(T_hot_ - T_cold_);
        return nusselt;
    }

    void storeStep(int step) {
        this->hdf_out_.reopen();
        this->hdf_out_.openGroup("/values/step" + std::to_string(step));
        this->hdf_out_.writeDoubleAttribute("time", this->time_);
        this->hdf_out_.writeDoubleArray("T", this->T_2_);
        this->hdf_out_.writeEigen("v", this->u_2_);
        auto nusselt_cold = this->calculateNusselt(edge_indices_[COLD]);
        this->hdf_out_.writeDoubleAttribute("nusselt", nusselt_cold.mean());
        this->hdf_out_.writeEigen("nusselt_cold", nusselt_cold);
        this->hdf_out_.writeEigen("nusselt_hot", this->calculateNusselt(edge_indices_[HOT]));
        this->hdf_out_.close();
    }

    void storeInitial() override {
        this->times_.push_back(this->time_);

        this->hdf_out_.reopen();
        this->hdf_out_.openGroup("/");
        this->hdf_out_.writeDouble2DArray("positions",
                                          this->domain_.positions(), true);
        this->hdf_out_.writeDoubleAttribute("Ra", this->Ra_);
        this->hdf_out_.writeDoubleAttribute("Pr", this->Pr_);
        this->hdf_out_.writeXML("", this->xml_in_, true);
        this->hdf_out_.close();

        storeStep(0);
    }

    void storeIntermediate(int step) override {
        this->times_.push_back(this->time_);
        storeStep(step);
    }

    void storeFinal(int step) override {
        this->times_.push_back(this->time_);

        this->tim_.addCheckPoint("end iteration");
        storeStep(step);
        this->hdf_out_.reopen();
        this->hdf_out_.openGroup("/");
        this->hdf_out_.writeTimer("timer", this->tim_);
        this->hdf_out_.writeDoubleArray("times", this->times_);
        this->hdf_out_.writeDoubleArray("viscosity", viscosity_);
        this->hdf_out_.writeEigen("pressure", solver_.solve(rhs_).head(domain_.size()));

        Range<Range<Eigen::Matrix<scal_t, dim, 1>>> grad_rows(dim);
        for (int i : domain_.all()) {
            for (int d = 0; d < dim; ++d) {
                grad_rows[d].push_back(vel_grad_[i].row(d));
            }
        }
        for (int d = 0; d < dim; ++d) {
            this->hdf_out_.writeDouble2DArray("grad_" + std::to_string(d), grad_rows[d]);
        }

        this->hdf_out_.close();
    }
};

#endif //STABILISEDNONNEWTONIAN_NONNEWTONIANCONVECTION_HPP
