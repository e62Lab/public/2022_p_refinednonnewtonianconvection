#ifndef STABILISEDNONNEWTONIAN_NONNEWTONIANCONVECTION_ACM_HPP
#define STABILISEDNONNEWTONIAN_NONNEWTONIANCONVECTION_ACM_HPP

#include <medusa/Medusa.hpp>
#include <iostream>
#include <Eigen/SparseCore>
#include <Eigen/SparseLU>
#include <medusa/IO.hpp>
#include <mutex>

#include "CalculationEnvironmentBase.hpp"

using namespace mm;

#define EPS 1e-10 //small number

template <int dim, int phs_order>
class NonNewtonianConvectionACM: public CalculationEnvironmentBase {
  protected:
    Timer tim_;
    XML &xml_in_;
    HDF hdf_out_;

    typedef double scal_t;
    typedef Vec<scal_t, dim> vec_t;
    typedef std::tuple<Der1s<dim>, Lap<dim>> operator_t;
    typedef RaggedShapeStorage<vec_t, operator_t> storage_t;

    scal_t h_, dt_, c_p_, rho_, lam_, mu_, power_index_, alpha_,
            beta_, T_ref_, T_cold_, T_hot_, Ra_, Pr_, courant_1_,
            courant_2_, max_visc_norm_, max_div_, compress_, v_ref_;
    Vec<scal_t, dim> g_;
    int stp_ = 0, max_p_iter_;

    DomainDiscretization<vec_t> domain_;
    storage_t storage_, neumann_storage_;
    ExplicitOperators<storage_t> op_scalar_, neumann_op_scalar_;
    ExplicitVectorOperators<storage_t> op_vector_, neumann_op_vector_;

    ScalarField<scal_t> T_1_, T_2_, viscosity_;
    ScalarField<scal_t> p_, p_0_, divergence_;
    VectorField<scal_t, dim> u_1_, u_2_;
    Range<Eigen::Matrix<scal_t, dim, dim>> vel_grad_;
    Range<VectorField<scal_t, dim>> viscous_stress_, viscous_stress_smooth_;
    Range<int> interior_;
    Range<Range<int>> edge_indices_;
    enum Edge : int {HOT, COLD, NEUTRAL, type_count};
    Range<int> ghost_mapping_;

    Range<scal_t> times_;

    Eigen::VectorXd rhs_p_, rhs_T_;
    Eigen::SparseLU<Eigen::SparseMatrix<scal_t>> solver_p_, solver_T_;


  public:
    explicit NonNewtonianConvectionACM(XML &xml_params)
            : xml_in_{xml_params}, domain_(UnknownShape<vec_t>()) {
        HDF::Mode mode = xml_in_.get<bool>("output.clear_hdf5")
                         ? HDF::DESTROY
                         : HDF::APPEND;
        hdf_out_.open(xml_in_.get<std::string>("output.hdf5_name"), "/", mode);
    }

    ~NonNewtonianConvectionACM() {
        hdf_out_.close();
    }

    void initialize() override {
        tim_.addCheckPoint("init");

        omp_set_num_threads(xml_in_.get<int>("sys.num_threads"));

        loadVariables();

        initializeTimeStepping(dt_, xml_in_.get<scal_t>("case.end_time"));
        initializeStorage(xml_in_.get<int>("output.count"));
        initializePrintout(xml_in_.get<int>("output.printout_interval"));

        constructDomain(xml_in_.get<scal_t>("case.height"));
        constructApproximation();
        setInitialValues();

        std::cout << "h=" << h_ << ", dt=" << dt_ << ", N=" << domain_.size() << std::endl;

        tim_.addCheckPoint("start iteration");
    }

    void loadVariables() {
        c_p_ = xml_in_.get<scal_t>("phy.c_p");
        rho_ = xml_in_.get<scal_t>("phy.rho");
        lam_ = xml_in_.get<scal_t>("phy.lam");
        mu_ = xml_in_.get<scal_t>("phy.mu");
        power_index_ = xml_in_.get<scal_t>("phy.power_index");
        beta_ = xml_in_.get<scal_t>("phy.beta");
        T_ref_ = xml_in_.get<scal_t>("phy.T_ref");
        T_hot_ = xml_in_.get<scal_t>("case.T_hot");
        T_cold_ = xml_in_.get<scal_t>("case.T_cold");
        auto g_0 = xml_in_.get<scal_t>("phy.g_0");

        alpha_ = lam_ / c_p_ / rho_;

        auto rayleigh = xml_in_.get<scal_t>("dimensionless.Ra");
        auto prandtl = xml_in_.get<scal_t>("dimensionless.Pr");
        auto height = xml_in_.get<scal_t>("case.height");
        if (xml_in_.get<int>("dimensionless.forceDimensionless")) {
            mu_ = prandtl * std::pow(alpha_, 2 - power_index_)
                  * rho_ * std::pow(height, power_index_ - 1);
            std::cout << "Prandtl number " << prandtl << " enforced by changing mu to " << mu_ << std::endl;
            xml_in_.set("phy.mu", mu_, true);
            beta_ = rayleigh * mu_ * std::pow(alpha_, power_index_)
                    * std::pow(height, -2 - power_index_) / rho_ / std::abs(g_0 *(T_hot_ - T_cold_));
            std::cout << "Rayleigh number " << rayleigh << " enforced by changing beta to " << beta_ << std::endl;
            xml_in_.set("phy.beta", beta_, true);
        }

        Ra_ = rho_ * beta_ * std::abs(g_0 *(T_hot_ - T_cold_)) * std::pow(height, power_index_ + 2) / std::pow(alpha_, power_index_) / mu_;
        Pr_ = mu_ * std::pow(alpha_, power_index_ - 2) * std::pow(height, 1 - power_index_) / rho_;

        h_ = xml_in_.get<scal_t>("num.h");
        dt_ = xml_in_.get<scal_t>("num.dt");
        if (xml_in_.exists("num.variable_dt") && xml_in_.get<bool>("num.variable_dt")) {
            courant_1_ = xml_in_.get<scal_t>("num.courant_1");
            courant_2_ = xml_in_.get<scal_t>("num.courant_2");
        } else {
            courant_1_ = -1;
            courant_2_ = -1;
        }
        max_visc_norm_ = xml_in_.get<scal_t>("num.max_norm");
        max_p_iter_ = xml_in_.get<int>("acm.max_p_iter");
        compress_ = xml_in_.get<scal_t>("acm.compressibility");
        v_ref_ = xml_in_.get<scal_t>("acm.velocity_reference");
        max_div_ = xml_in_.get<scal_t>("acm.max_div");

        g_.setZero();
        g_[dim - 1] = g_0; // gravity is defined to work in the last dimension
    }

    virtual void constructDomain(scal_t height) {}

    virtual void constructApproximation() {
        tim_.addCheckPoint("approximation");
        Polyharmonic<scal_t, phs_order> phs;
        Monomials<vec_t> mon(xml_in_.get<scal_t>("num.mon_order"));
        int support_size = std::round(xml_in_.get<scal_t>("num.support_size_factor") * mon.size()) + 1;
        RBFFD<decltype(phs), vec_t, ScaleToClosest> appr(phs, mon);
        operator_t ops;
        std::cout << "Explicit momentum and heat equations with artificial compressibility" << std::endl;
        std::cout << "Domain size=" << domain_.size() << ", Support size=" << support_size << std::endl;


        domain_.removeNodes(domain_.types() == 0);

        for (int i : domain_.all()) {
            domain_.support(i).clear();
        }
        domain_.findSupport(FindClosest(support_size).forNodes(domain_.boundary()).searchAmong(domain_.interior()).forceSelf(true));
        neumann_storage_.resize(domain_.supportSizes());
        computeShapes(domain_, appr, domain_.boundary(), ops, &neumann_storage_);
        neumann_op_scalar_ = neumann_storage_.explicitOperators();
        neumann_op_vector_ = neumann_storage_.explicitVectorOperators();

        for (int i : domain_.all()) {
            domain_.support(i).clear();
        }
        domain_.findSupport(FindClosest(support_size));

        storage_.resize(domain_.supportSizes());
        computeShapes(domain_, appr, domain_.all(), ops, &storage_);
        op_scalar_ = storage_.explicitOperators();
        op_vector_ = storage_.explicitVectorOperators();
    }

    virtual void setInitialValues() {
        auto T_cold = xml_in_.template get<scal_t>("case.T_cold");
        auto T_hot = xml_in_.template get<scal_t>("case.T_hot");
        T_1_.resize(domain_.size());
        T_1_ = xml_in_.template get<scal_t>("case.T_init");
        auto borders = domain_.shape().bbox();
        scal_t length = borders.second[0] - borders.first[0];
        T_1_[edge_indices_[COLD]] = T_cold;
        T_1_[edge_indices_[HOT]] = T_hot;
        u_1_.resize(domain_.size(), dim);
        u_1_.setZero();
        if (xml_in_.exists("case.load_initial")
            && xml_in_.template get<scal_t>("case.load_initial")) {
            loadInitialValues();
        }
        T_2_ = T_1_;
        u_2_ = u_1_;

        viscous_stress_.resize(dim);
        viscous_stress_smooth_.resize(dim);
        for (int i = 0; i < dim; ++i) {
            viscous_stress_[i].resize(domain_.size(), dim);
            viscous_stress_[i].setZero();
            viscous_stress_smooth_[i].resize(domain_.size(), dim);
            viscous_stress_smooth_[i].setZero();
        }

        viscosity_.resize(domain_.size());
        vel_grad_.resize(domain_.size());

        p_0_.resize(domain_.size());
        p_0_.setZero();
        p_ = p_0_;
        divergence_.resize(domain_.size());
        divergence_.setZero();
    }

    void loadInitialValues() {
        int neighbours = 10; // TODO: set as param if neccessary
        auto load_file = xml_in_.template get<std::string>("case.load_from");
        HDF hdf_in(load_file, HDF::READONLY);
        auto positions = hdf_in.readDouble2DArray("positions");
        auto velocities = hdf_in.readEigen("velocities");
        auto temperatures = hdf_in.readEigen("temperatures");
        Range<vec_t> pos;
        Range<scal_t> v_x, v_y, T;
        for (unsigned i = 0; i < positions.size(); ++i) {
            pos.push_back(vec_t::Zero());
            for (int j = 0; j < dim; ++j) {
                pos.back()[j] = positions[i][j]; // TODO: Implement a more general solution.
            }
            v_x.push_back(velocities(i, 0));
            v_y.push_back(velocities(i, 1));
            T.push_back(temperatures(i));
        }
        SheppardInterpolant<vec_t, scal_t> intp_v_x(pos, v_x);
        SheppardInterpolant<vec_t, scal_t> intp_v_y(pos, v_y);
        SheppardInterpolant<vec_t, scal_t> intp_T(pos, T);

        #pragma omp parallel for default(none) shared(intp_v_x, intp_v_y, intp_T, neighbours)
        for (int intIdx = 0; intIdx < interior_.size(); ++intIdx) {
            int i = interior_[intIdx];
            auto p = domain_.pos(i);
            T_1_(i) = intp_T(p, neighbours);
            u_1_(i, 0) = intp_v_x(p, neighbours);
            u_1_(i, 1) = intp_v_y(p, neighbours);
        }
        #pragma omp parallel for default(none) shared(intp_v_x, intp_v_y, intp_T, neighbours)
        for (int intIdx = 0; intIdx < edge_indices_[NEUTRAL].size(); ++intIdx) {
            int i = edge_indices_[NEUTRAL][intIdx];
            auto p = domain_.pos(i);
            T_1_(i) = intp_T(p, neighbours);
        }

        T_2_ = T_1_;
        u_2_ = u_1_;
    }

    void timeStep() override {
        scal_t max_norm = 0, max_visc = 0;
        scal_t dt_1 = 1e-4, dt_2 = 1e-4;
        #pragma omp parallel for default(none) schedule(static) \
                reduction(max : max_norm, max_visc) reduction(min : dt_1, dt_2)
        for (int ii = 0; ii < domain_.size(); ++ii) {
            int i = ii;
            max_norm = std::max(u_1_[i].norm(), max_norm);
            vel_grad_[i] = op_vector_.grad(u_1_, i);
            scal_t norm = (vel_grad_[i] + vel_grad_[i].transpose()).squaredNorm() / 2;
            norm = std::min(std::max(norm, 1 / max_visc_norm_), max_visc_norm_);
            viscosity_[i] = mu_ * std::pow(norm, (power_index_ - 1) / 2);
            max_visc = std::max(max_visc, viscosity_[i]);

            if (courant_1_ > 0) {
                scal_t dx = (domain_.supportNode(i, 1) - domain_.supportNode(i, 0)).norm();
                dt_1 = std::min(dt_1, courant_1_ * dx / u_1_[i].norm());
                dt_2 = std::min(dt_2, courant_2_ * dx * dx * rho_ / viscosity_[i] / 2);
            }
        }

        if (courant_1_ > 0) {
            dt_ = std::min(dt_1, dt_2);
            this->time_step_ = dt_;
        }

        #pragma omp parallel for default(none) schedule(static)
        for (int i = 0; i < interior_.size(); ++i) {
            int c = interior_[i];
            u_2_[c] = u_1_[c] + dt_ * (
                    ((vel_grad_[c] + vel_grad_[c].transpose()) * op_scalar_.grad(viscosity_, c)
                     + viscosity_[c] * op_vector_.lap(u_1_, c)) / rho_
                    - vel_grad_[c] * u_1_[c]
                    + g_ * (-beta_ * (T_1_[c] - T_ref_))
            );
        }

        max_norm = 0;
        #pragma omp parallel for default(none) schedule(static) reduction(max : max_norm)
        for (int i = 0; i < interior_.size(); ++i) {
            int c = interior_[i];
            u_1_[c] = u_2_[c] - dt_ / rho_ * op_scalar_.grad(p_0_, c);
            max_norm = std::max(u_1_[c].norm(), max_norm);
        }

        for (int acm_iter = 0; acm_iter < max_p_iter_; ++acm_iter) {
            // Speed of sound
            scal_t C = compress_ * std::max(max_norm, v_ref_);

            // Mass continuity
            scal_t max_div = 0;
            #pragma omp parallel for shared(C) schedule(static) reduction(max : max_div) default(none)
            for (int ii = 0; ii < interior_.size(); ++ii) {
                int i = interior_[ii];
                divergence_[i] = op_vector_.div(u_1_, i);
                max_div = std::max(std::abs(divergence_[i]), max_div);
                p_[i] = p_0_[i] - C * C * dt_ * rho_ * divergence_[i];
            }
            for (int i = 0; i < edge_indices_.size(); ++i) {
                for (int j : edge_indices_[i]) {
                    p_[j] = neumann_op_scalar_.neumann(p_, j, domain_.normal(j), 0);
                }
            }
            p_0_ = p_;

            #pragma omp parallel for default(none) schedule(static)
            for (int i = 0; i < interior_.size(); ++i) {
                int c = interior_[i];
                u_1_[c] = u_2_[c] - dt_ / rho_ * op_scalar_.grad(p_0_, c);
            }

            if (max_div < max_div_) break;
        }

        // explicit heat transfer
        #pragma omp parallel for default(none) schedule(static)
        for (int i = 0; i < interior_.size(); ++i) {
            int c = interior_[i];
            T_2_[c] = T_1_[c] + dt_ * alpha_ * op_scalar_.lap(T_1_, c)
                      - dt_ * u_1_[c].dot(op_scalar_.grad(T_1_, c));
        }

        #pragma omp parallel for default(none) schedule(static)
        for (int i = 0; i < edge_indices_[NEUTRAL].size(); ++i) {
            int c = edge_indices_[NEUTRAL][i];
            T_2_[c] = neumann_op_scalar_.neumann(T_2_, c, domain_.normal(c), 0.0);
        }
        T_1_ = T_2_;
    }

    virtual bool terminateAtStoreIntermediate() {
        for ( int i = 0; i < u_1_.rows(); ++i) {
            for (int j = 0; j < dim; ++j) {
                if (std::isnan(u_1_.coeff(i, j))) {
                    std::cout << "nan node " << i << std::endl;
                    return true;
                }
            }
        }
        return false;
    };

    std::string printout() override {
        scal_t max_div = 0;
        for (int i : interior_) {
            max_div = std::max(max_div, std::abs(op_vector_.div(u_1_, i)));
        }
        auto nusselt_cold = calculateNusselt(edge_indices_[COLD]);
        std::stringstream ss;
        ss << std::setprecision(5) << "mean Nu:" << nusselt_cold.mean()
           << ", max u:" << u_1_.maxCoeff() << ", mean u:" << u_1_.mean() << ", max div: " << max_div;
        return ss.str();
    }

    ScalarField<scal_t> calculateNusselt(indexes_t where) {
        // TODO: add L (not needed as char L = 1, reconsider)
        ScalarField<scal_t> nusselt(where.size());
        int idx = 0;
        for (auto i : where) {
            nusselt(idx++) = std::abs(op_scalar_.d1(T_2_, 0, i));
        }
        nusselt /= std::abs(T_hot_ - T_cold_);
        return nusselt;
    }

    void storeStep(int step) {
        this->hdf_out_.reopen();
        this->hdf_out_.openGroup("/values/step" + std::to_string(step));
        scal_t max_div = 0;
        for (int i : interior_) {
            max_div = std::max(max_div, std::abs(op_vector_.div(u_1_, i)));
        }
        this->hdf_out_.writeDoubleAttribute("max_div", this->time_);
        this->hdf_out_.writeDoubleAttribute("time", this->time_);
        this->hdf_out_.writeDoubleAttribute("dt", dt_);
        this->hdf_out_.writeDoubleArray("T", T_2_);
        this->hdf_out_.writeEigen("v", u_1_);
        auto nusselt_cold = this->calculateNusselt(edge_indices_[COLD]);
        this->hdf_out_.writeDoubleAttribute("nusselt", nusselt_cold.mean());
        this->hdf_out_.writeEigen("nusselt_cold", nusselt_cold);
        this->hdf_out_.writeEigen("nusselt_hot", this->calculateNusselt(edge_indices_[HOT]));
        this->hdf_out_.close();
    }

    void storeInitial() override {
        this->times_.push_back(this->time_);

        this->hdf_out_.reopen();
        this->hdf_out_.openGroup("/");
        this->hdf_out_.writeDouble2DArray("positions",
                                          this->domain_.positions(), true);
        this->hdf_out_.writeDoubleAttribute("Ra", this->Ra_);
        this->hdf_out_.writeDoubleAttribute("Pr", this->Pr_);
        this->hdf_out_.writeXML("", this->xml_in_, true);
        this->hdf_out_.close();

        storeStep(0);
    }

    void storeIntermediate(int step) override {
        this->times_.push_back(this->time_);
        storeStep(step);
    }

    void storeFinal(int step) override {
        this->times_.push_back(this->time_);

        this->tim_.addCheckPoint("end iteration");
        storeStep(step);
        this->hdf_out_.reopen();
        this->hdf_out_.openGroup("/");
        this->hdf_out_.writeDoubleArray("times", this->times_);
        this->hdf_out_.writeDoubleArray("viscosity", viscosity_);
        this->hdf_out_.writeEigen("pressure", p_0_);
        this->hdf_out_.writeEigen("divergence", divergence_);
        this->hdf_out_.writeTimer("timer", this->tim_);
        this->hdf_out_.close();
    }
};

#endif //STABILISEDNONNEWTONIAN_NONNEWTONIANCONVECTION_ACM_HPP
