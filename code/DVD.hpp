#ifndef NONNEWTONIANTHERMOFLUIDREFINEMENT_DVD_H
#define NONNEWTONIANTHERMOFLUIDREFINEMENT_DVD_H

#include <medusa/Medusa.hpp>
#include <iostream>
#include <Eigen/SparseCore>
#include <Eigen/SparseLU>
#include <medusa/IO.hpp>
#include <mutex>

#include "NonNewtonianConvection.hpp"
#include "NonNewtonianConvection_ACM.hpp"
//#include "NonNewtonianConvection_LessP.hpp"

using namespace mm;

#define EPS 1e-10 //small number
//#define PARENT NonNewtonianConvection
#define PARENT NonNewtonianConvectionACM
//#define PARENT NonNewtonianConvectionLessP

template <int dim, int phs_order>
class DVD: public PARENT<dim, phs_order> {
    using PARENT<dim, phs_order>::PARENT;

protected:
    typedef typename PARENT<dim, phs_order>::scal_t scal_t;
    typedef typename PARENT<dim, phs_order>::vec_t vec_t;

    void constructDomain(scal_t height) {
        vec_t beg = vec_t::Zero();
        vec_t end = vec_t::Constant(1); // domain width of 1
        end(end.size() - 1) = height; // domain height set in params
        BoxShape<vec_t> box(beg, end);

        if (this->xml_in_.template get<bool>("fill.enabled")) {
            std::function<scal_t (const vec_t&)> density_fn;
            if (this->xml_in_.template get<bool>("fill.variable_density")) {
                auto max_step = this->xml_in_.template get<scal_t>("fill.max_step");
                if (this->xml_in_.exists("fill.refine_ratio") && this->xml_in_.template get<scal_t>("fill.refine_ratio") > 0) {
                    max_step = std::min(max_step, this->h_ * this->xml_in_.template get<scal_t>("fill.refine_ratio"));
                }
                auto dense_width = this->xml_in_.template get<scal_t>("fill.dense_band");
                density_fn = [=](const vec_t& p) {
                    // square domain linear density change
                    auto borders = box.bbox();
                    scal_t max_distance = (borders.second - borders.first).maxCoeff() / 2
                                          - dense_width;
                    scal_t r = std::min((borders.first - p).cwiseAbs().minCoeff(),
                                        (borders.second - p).cwiseAbs().minCoeff());
                    if (r < dense_width) return this->h_;
                    return this->h_ + ((r - dense_width) / max_distance) * (max_step - this->h_);
                };
            } else {
                density_fn = [&](const vec_t& p) {return this->h_;};
            }

            this->domain_ = box.discretizeBoundaryWithDensity(density_fn);

            if (this->xml_in_.template get<bool>("fill.neumann_layer")) {
                determineBoundaryTypes();
                for (int edgeIdx : this->edge_indices_[this->NEUTRAL]) {
                    // exclude corner and edge nodes with "weird" normals
                    if (this->domain_.normal(edgeIdx).cwiseAbs().maxCoeff() < 1) continue;

                    auto candidatePos = this->domain_.pos(edgeIdx)
                                        - this->domain_.normal(edgeIdx) * this->h_;
                    bool validPosition = true;
                    for (int existingIdx : this->domain_.interior()) {
                        if ((this->domain_.pos(existingIdx) - candidatePos).norm() < this->h_ - EPS) {
                            validPosition = false;
                            break;
                        }
                    }
                    if (validPosition) this->domain_.addInternalNode(candidatePos, 1);
                }
            }

            GeneralFill<vec_t> fill;
            fill.seed(this->xml_in_.template get<scal_t>("fill.seed"));
            fill(this->domain_, density_fn);

            if (this->xml_in_.template get<bool>("relax.enabled")) {
                if (this->xml_in_.template get<bool>("fill.variable_density")) {
                    std::cout << "WARNING: Node relaxation counteracts variable density fill." << std::endl;
                }
                BasicRelax relax;
                relax.initialHeat(this->xml_in_.template get<scal_t>("relax.initial_heat"))
                        .finalHeat(this->xml_in_.template get<scal_t>("relax.final_heat"))
                        .iterations(this->xml_in_.template get<int>("relax.iter_num"))
                        .numNeighbours(this->xml_in_.template get<int>("relax.num_neighbours"))
                        .projectionType(BasicRelax::DO_NOT_PROJECT);
                relax(this->domain_);
            }

            enforceDistanceToBoundary(this->h_ / 2);
        } else {
            this->domain_ = box.discretizeWithStep(this->h_);
        }

        if (this->xml_in_.template get<bool>("domain.no_corner")) {
            removeCornerNodes();
            // boundary indexing needs to be recalculated due to node removal
        }
        determineBoundaryTypes();
        this->interior_ = this->domain_.interior();
    }

    void removeCornerNodes() {
        auto borders = this->domain_.shape().bbox();
        Range<int> corner = this->domain_.positions().filter([&](const vec_t &p) {
            // remove nodes that are EPS close to more than 1 border
            return (((p - borders.first) < EPS).size() + ((borders.second - p) < EPS).size()) > 1;
        });
        this->domain_.removeNodes(corner);
    }

    void enforceDistanceToBoundary(float distance) {
        auto borders = this->domain_.shape().bbox();
        Range<vec_t> interiorPos = this->domain_.positions()[this->domain_.interior()];
        Range<int> closeToBorder = interiorPos.filter(
                [&](const vec_t &p) {
                    return (((p - borders.first) < distance).size()
                            + ((borders.second - p) < distance).size()) > 0;
                });
        Range<int> erronousIndices = ((Range<int>)this->domain_.interior())[closeToBorder];
        this->domain_.removeNodes(erronousIndices);
    }

    void determineBoundaryTypes() {
        auto borders = this->domain_.shape().bbox();
        auto closestOther = [&](const vec_t& p) {
            return std::min((p - borders.first)(Eigen::lastN(dim - 1)).minCoeff(),
                            (borders.second - p)(Eigen::lastN(dim - 1)).minCoeff());
        };

        Range<int> left_idx = this->domain_.positions().filter([&](const vec_t& p) {
            return (p[0] - borders.first[0] < EPS) && closestOther(p) > EPS; });
        Range<int> right_idx = this->domain_.positions().filter([&](const vec_t& p) {
            return (borders.second[0] - p[0] < EPS) && closestOther(p) > EPS; });
        Range<int> boundary = this->domain_.boundary();
        Range<int> other_idx = boundary.filter([&](const int idx) {
            for (auto i : left_idx) if (idx == i) return false;
            for (auto i : right_idx) if (idx == i) return false;
            return true; });

        this->edge_indices_.resize(this->type_count);
        this->edge_indices_[this->NEUTRAL] = boundary[other_idx];
        this->edge_indices_[this->COLD] = left_idx;
        this->edge_indices_[this->HOT] = right_idx;
    }

};

#endif //NONNEWTONIANTHERMOFLUIDREFINEMENT_DVD_H
