#include <medusa/Medusa.hpp>
#include "PorousCavity.hpp"

using namespace mm;

constexpr int phs_order = 3;

int main(int arg_num, char* arg[]) {
    const std::string parameter_file = "../params/porous_params.xml";

    // delete old data file instance
    {
        XML params(parameter_file);
        HDF hdf_file(params.get<std::string>("output.hdf5_name"), HDF::DESTROY);
        omp_set_num_threads(params.get<int>("sys.num_threads"));
    }

    XML params(parameter_file);
    int dim = params.get<int>("case.dim");
    if (dim == 2) {
        PorousCavity<2, phs_order> solution(params);
        solution.run();
    } else if (dim == 3) {
        PorousCavity<3, phs_order> solution(params);
        solution.run();
    } else {
        std::cout << "dim=" << dim << " is not supported.";
    }
}

