#ifndef NONNEWTONIANTHERMOFLUIDREFINEMENT_POROUSCAVITY_HPP
#define NONNEWTONIANTHERMOFLUIDREFINEMENT_POROUSCAVITY_HPP

#include <medusa/Medusa.hpp>
#include <iostream>
#include "NonNewtonianConvection_ACM.hpp"

#include "obstructions/CircularObstruction.hpp"
#include "obstructions/InvertedRectangularObstruction.hpp"
#include "obstructions/ObstructedDomain.hpp"

using namespace mm;

#define EPS 1e-10 //small number
#define PARENT NonNewtonianConvectionACM


template <int dim, int phs_order>
class PorousCavity: public PARENT<dim, phs_order> {
    using PARENT<dim, phs_order>::PARENT;

protected:
    typedef typename PARENT<dim, phs_order>::scal_t scal_t;
    typedef typename PARENT<dim, phs_order>::vec_t vec_t;

    void constructDomain(scal_t height) override {
        placeObstructions(this->domain_, this->xml_in_);

        if (this->xml_in_.template get<bool>("domain.no_corner")) removeCornerNodes();

        this->edge_indices_.resize(this->type_count);
        determineBoundaryTypes(this->domain_, this->edge_indices_);

        this->interior_ = this->domain_.interior();
    }


    void placeObstructions(DomainDiscretization<vec_t>& domain, XML& param_file) {
        auto seed = param_file.get<int>("fill.seed");
        scal_t width = 1;
        scal_t length = 1; // TODO: set as param
        scal_t height = 1;
        auto radius = param_file.get<scal_t>("case.radius");
        auto buffer_width = param_file.get<scal_t>("case.buffer_width");
        auto minimum_distance = param_file.get<scal_t>("case.min_sphere_distance");
        auto candidate_distance = param_file.get<scal_t>("case.candidate_distance");
        auto candidate_probability = param_file.get<scal_t>("case.candidate_probability");

        std::mt19937 rng(seed);
        std::uniform_real_distribution<scal_t> random(0, 1);

        vec_t beg = vec_t::Constant(-5 * radius);
        vec_t end = vec_t::Constant(5 * radius);
        if (param_file.get<bool>("case.horizontal_gradient")) {
            beg[0] = buffer_width - radius - minimum_distance;
            end[0] = length - buffer_width + radius + minimum_distance;
            end[1] += width;
        } else {
            scal_t last_len = (dim == 3) ? height : width;
            beg[dim - 1] = buffer_width - radius - minimum_distance;
            end[dim - 1] = last_len - buffer_width + radius + minimum_distance;
            end[0] += length;
        }
        if (dim == 3) {
            // Avoid spheres intersecting with side walls for better visibility
            beg[0] = -radius + minimum_distance;
            end[0] = length + radius - minimum_distance;
            beg[1] = -radius + minimum_distance;
            end[1] = width + radius - minimum_distance;
        }

        domain = BoxShape<vec_t>(beg, end).discretizeBoundaryWithStep(2 * radius + minimum_distance);
        GeneralFill<vec_t> gf;
        gf.seed(seed);
        gf(domain, 2 * radius + minimum_distance); // Using the general fill algorithm to place equidistant obstructions in a smaller domain.

        beg = vec_t::Zero();
        end = vec_t::Constant(height);
        end[0] = length;
        end[1] = width;

        InvertedRectangularObstruction<vec_t> border_obstruction(beg, end);
        ObstructedDomain<vec_t> obstructed_domain(border_obstruction, -3);
        auto borders = border_obstruction.shape()->bbox();
        int N =  0;
        Range<vec_t> obstruction_positions = domain.positions()[domain.interior()];
        for (auto p : obstruction_positions) {
            CircularObstruction<vec_t> obstruction(p, radius);
            obstructed_domain.addObstruction(obstruction);
        }

        domain.clear(); // Clear obstruction placing domain.

        auto boundary_fn = [&](const vec_t &p, const ObstructedDomain<vec_t> &o, int i) {
            return boundaryDensity(p, o, i);
        };
        auto interior_fn = [&](const vec_t &p) {
            return interiorDensity(p, obstructed_domain);
        };
        obstructed_domain.discretizeWithDensity(domain, boundary_fn);
        gf.seed(seed);
        gf(domain, interior_fn);

        BasicRelax relax;
        relax.initialHeat(param_file.get<scal_t>("relax.initial_heat"))
                .finalHeat(param_file.get<scal_t>("relax.final_heat"))
                .iterations(param_file.get<int>("relax.iter_num"))
                .numNeighbours(param_file.get<int>("relax.num_neighbours"));
        relax.onlyNodes(domain.interior());
        relax(domain);

        Range<int> too_close;
        auto min_distance = param_file.get<scal_t>("fill.min_step") / 2;
        auto max_h = this->xml_in_.template get<scal_t>("num.h");
        for (auto i : domain.interior()) {
            if (obstructed_domain.distance(domain.pos(i)) < min_distance) {
                too_close.push_back(i);
            }
            if (dim == 3) {
                if (1 - domain.pos(i, dim-1) < max_h / 3) {
                    too_close.push_back(i);
                }
                if (domain.pos(i, dim-1) < max_h / 3) {
                    too_close.push_back(i);
                }
            }
        }
        domain.removeNodes(too_close);

        if (dim == 3) { // Weird relax behaviour pushing nodes too close in 3D, resolve with a better algo
            min_distance = 0.75 * param_file.get<scal_t>("fill.min_step");
            KDTree<vec_t> tree(domain.positions());
            too_close.clear();
            for (auto i : domain.interior()) {
                scal_t d2 = tree.query(domain.pos(i), 2).second[1];
                if (d2 < min_distance * min_distance) too_close.push_back(i);
            }
            std::cout << "Removed due to closeness: " << too_close.size() << std::endl;
            domain.removeNodes(too_close);
        }

        scal_t min_h = max_h;
        KDTree<vec_t> tree(domain.positions());
        scal_t min_d = 100;
        int min_idx;
        for (auto i : domain.interior()) {
            scal_t d = obstructed_domain.distance(domain.pos(i));
            if (d < min_d) {
                min_d = d;
                min_idx = i;
            }
            scal_t d2 = tree.query(domain.pos(i), 2).second[1];
            if (d2 < min_h) min_h = d2;
        }
        std::cout << "Min internodal distance: " << std::sqrt(min_h) << std::endl;
        std::cout << "Min node-wall dist at idx " << min_idx << " is: " << min_d << std::endl;
    }

    scal_t boundaryDensity(const vec_t &p, const ObstructedDomain<vec_t> &obstructions, int my_type) {
        auto k = this->xml_in_.template get<scal_t>("fill.boundary_dist_ratio");
        auto max_step = this->xml_in_.template get<scal_t>("num.h");
        auto min_step = this->xml_in_.template get<scal_t>("fill.min_step");
        return std::max(std::min(max_step, obstructions.distanceToOtherType(p, my_type) / k), min_step);
    }


    scal_t interiorDensity(const vec_t &p, const ObstructedDomain<vec_t> &obstructions) {
        int first_type;
        // finds closest (boundary types are < 0) & second closest boundary (inefficient & simple)
        obstructions.distanceToOtherType(p, 0, first_type);
        scal_t dist = obstructions.distanceToOtherType(p, first_type);
        auto k = this->xml_in_.template get<scal_t>("fill.interior_dist_ratio");
        auto max_step = this->xml_in_.template get<scal_t>("num.h");
        auto min_step = this->xml_in_.template get<scal_t>("fill.min_step");
        return std::max(std::min(max_step, dist / k), min_step);
    }

    void determineBoundaryTypes(DomainDiscretization<vec_t>& domain, Range<Range<int>>& indices) {
        auto borders = domain.shape().bbox();
        auto closestOther = [&](const vec_t& p) {
            return std::min((p - borders.first)(Eigen::lastN(dim - 1)).minCoeff(),
                            (borders.second - p)(Eigen::lastN(dim - 1)).minCoeff());
        };

        Range<int> cold_idx, hot_idx;
        if (this->xml_in_.template get<bool>("case.horizontal_gradient")) {
            cold_idx = this->domain_.positions().filter([&](const vec_t& p) {
                return (p[0] - borders.first[0] < EPS) && closestOther(p) > EPS;
            });
            hot_idx = this->domain_.positions().filter([&](const vec_t& p) {
                return (borders.second[0] - p[0] < EPS) && closestOther(p) > EPS;
            });
        } else {
            hot_idx = this->domain_.positions().filter([&](const vec_t& p) {
                return (p[dim - 1] < EPS);
            });
            cold_idx = this->domain_.positions().filter([&](const vec_t& p) {
                return (borders.second[dim - 1] - p[dim - 1] < EPS);
            });
        }
        Range<int> boundary = this->domain_.boundary();
        Range<int> other_idx = boundary.filter([&](const int idx) {
            for (auto i : cold_idx) if (idx == i) return false;
            for (auto i : hot_idx) if (idx == i) return false;
            return true; });

        indices[this->NEUTRAL] = boundary[other_idx];
        indices[this->COLD] = cold_idx;
        indices[this->HOT] = hot_idx;

        domain.types()[cold_idx] = -1;
        domain.types()[hot_idx] = -2;
        domain.types()[other_idx] = -3;
    }

    void removeCornerNodes() {
        auto borders = this->domain_.shape().bbox();
        Range<int> corner = this->domain_.positions().filter([&](const vec_t &p) {
            // remove nodes that are EPS close to more than 1 border
            return (((p - borders.first) < EPS).size() + ((borders.second - p) < EPS).size()) > 1;
        });
        this->domain_.removeNodes(corner);
    }

    void enforceDistanceToBoundary(float distance) {
        auto borders = this->domain_.shape().bbox();
        Range<vec_t> interiorPos = this->domain_.positions()[this->domain_.interior()];
        Range<int> closeToBorder = interiorPos.filter(
                [&](const vec_t &p) {
                    return (((p - borders.first) < distance).size()
                            + ((borders.second - p) < distance).size()) > 0;
                });
        Range<int> erronousIndices = ((Range<int>)this->domain_.interior())[closeToBorder];
        this->domain_.removeNodes(erronousIndices);
    }

};

#endif //NONNEWTONIANTHERMOFLUIDREFINEMENT_POROUSCAVITY_HPP
