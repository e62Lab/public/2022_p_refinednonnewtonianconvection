#ifndef RESTRICTED_CHANNEL_FLOW_INVERTEDRECTANGULAROBSTRUCTION_HPP
#define RESTRICTED_CHANNEL_FLOW_INVERTEDRECTANGULAROBSTRUCTION_HPP

#include "RectangularObstruction.hpp"

template <class vec_t>
class InvertedRectangularObstruction : public RectangularObstruction<vec_t> {
    using RectangularObstruction<vec_t>::RectangularObstruction;

public:
    double distance(const vec_t& p) const override {
        return -RectangularObstruction<vec_t>::distance(p);
    }


    bool overlap(const Obstruction<vec_t>& other) const override {
        // TODO: Implement overlap functionality
        throw std::runtime_error("RectangleObstruction.overlap() has not been implemented yet.");
    }

    std::unique_ptr<Obstruction<vec_t>> asPtr() override {
        return std::make_unique<InvertedRectangularObstruction<vec_t>>(this->diag_beg_,
                                                                       this->diag_end_);
    }
};

#endif //RESTRICTED_CHANNEL_FLOW_INVERTEDRECTANGULAROBSTRUCTION_HPP
