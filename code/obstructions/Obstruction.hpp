#ifndef RESTRICTED_CHANNEL_FLOW_OBSTRUCTION_HPP
#define RESTRICTED_CHANNEL_FLOW_OBSTRUCTION_HPP

#include <utility>
#include <typeinfo>
#include <medusa/Medusa.hpp>

template <class vec_t>
class Obstruction {
private:
    virtual bool isEqual(const Obstruction<vec_t>& other) const = 0;

public:
    virtual ~Obstruction() = default;

//    virtual std::unique_ptr<Obstruction<vec_t>> placeRandomly() = 0;

    // Distances to points within the obstruction should be given as a negative value.
    virtual double distance(const vec_t& p) const = 0;


    virtual bool overlap(const Obstruction<vec_t>& other) const = 0;


    virtual std::pair<vec_t, vec_t> bbox() const = 0;


    virtual std::unique_ptr<mm::DomainShape<vec_t>> shape() const = 0;


    virtual std::unique_ptr<Obstruction<vec_t>> asPtr() = 0;


    bool operator==(const Obstruction<vec_t>& other) const {
        return typeid(*this) == typeid(other) && isEqual(other);
    }
};

#endif //RESTRICTED_CHANNEL_FLOW_OBSTRUCTION_HPP
