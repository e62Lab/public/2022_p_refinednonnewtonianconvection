#ifndef RESTRICTED_CHANNEL_FLOW_OBSTRUCTEDDOMAIN_HPP
#define RESTRICTED_CHANNEL_FLOW_OBSTRUCTEDDOMAIN_HPP

#include <list>
#include <limits>
#include <functional>
#include "Obstruction.hpp"
#include "ObstructionCluster.hpp"

template <class vec_t>
class ObstructedDomain {
private:
    std::unique_ptr<Obstruction<vec_t>> outer_border_;
    std::list<ObstructionCluster<vec_t>> obstruction_list_;
    int border_type_;


public:
    explicit ObstructedDomain(Obstruction<vec_t>& outer_border, int border_type)
        : outer_border_{outer_border.asPtr()}, border_type_{border_type} {}

    template <class obstruction_t>
    void addObstruction(double min_size, double max_size) {
        vec_t bbox_reduction = vec_t::Zero();
        addObstruction<obstruction_t>(min_size, max_size, bbox_reduction);
    }

    template <class obstruction_t>
    void addObstruction(double min_size, double max_size, vec_t& bbox_reduction) {
        auto reduced_bbox = bbox();
        reduced_bbox.first += bbox_reduction;
        reduced_bbox.second -= bbox_reduction;
        auto validation_fn = [](const vec_t& p) { return true; };
        obstruction_t new_obstruction(reduced_bbox, validation_fn, min_size, max_size);
        addObstruction(new_obstruction);
    }
    // TODO: provide additional overloads with more options regarding the object placement
    // (eg. position validating function,...)

    void addObstruction(Obstruction<vec_t>& obstruction) {
        int node_type = border_type_;
        auto it = obstruction_list_.begin();
        auto add_to = obstruction_list_.end();
        while (it != obstruction_list_.end()) {
            if (it->getType() < node_type) node_type = it->getType();
            if (it->overlap(obstruction)) {
                if (add_to == obstruction_list_.end()) {
                    add_to = it;
                    it->join(obstruction);
                    if (obstruction.overlap(*outer_border_)) it->setBorderOverlap(true);
                } else {
                    if (it->getBorderOverlap()) add_to->setBorderOverlap(true);
                    add_to->join(*it);
                    it = obstruction_list_.erase(it);
                    continue;
                }
            }
            ++it;
        }
        if (add_to == obstruction_list_.end()) {
            obstruction_list_.emplace_back(obstruction, --node_type);
            if (obstruction.overlap(*outer_border_)) obstruction_list_.back().setBorderOverlap(true);
        }
    }

    double distanceToOtherType(const vec_t& p, int my_type,
                               int& other_type, bool ignore_border_overlap=false) const {
        double min_distance = std::numeric_limits<double>::infinity();
        other_type = std::numeric_limits<int>::max();
        bool type_border_overlap = false;
        if (!ignore_border_overlap) {
            for (const auto& obstruction : obstruction_list_) {
                if (my_type == obstruction.getType()) {
                    type_border_overlap = obstruction.getBorderOverlap();
                    break;
                }
            }
        }
        if (my_type != border_type_ && (ignore_border_overlap || !type_border_overlap)) {
            min_distance = outer_border_->distance(p);
            other_type = border_type_;
        }
        for (const auto& obstruction : obstruction_list_) {
            if (my_type != obstruction.getType()) {
                if (!ignore_border_overlap
                      && (my_type == border_type_)
                      && obstruction.getBorderOverlap()) continue;
                double distance = obstruction.distance(p);
                if (distance < min_distance) {
                    min_distance = distance;
                    other_type = obstruction.getType();
                }
            }
        }
        return min_distance;
    }

    double distanceToOtherType(const vec_t& p, int my_type) const {
        int other_type;
        return distanceToOtherType(p, my_type, other_type);
    }

    double distance(const vec_t& p) const {
        return distanceToOtherType(p, 0);
    }

    double distance(const vec_t& p, int& other_type) const {
        return distanceToOtherType(p, 0, other_type);
    }


    void discretizeWithDensity(mm::DomainDiscretization<vec_t>& domain,
                               const std::function<double (const vec_t&, const ObstructedDomain<vec_t>&, int)>& border_density_fn) {
        auto outside_fn = [&](const vec_t& p) { return border_density_fn(p, *this, border_type_); };
        domain = outer_border_->shape()->discretizeBoundaryWithDensity(outside_fn, border_type_);
        for (auto& obstruction : obstruction_list_) {
            auto obstruction_fn = [&](const vec_t& p) {
                return border_density_fn(p, *this, obstruction.getType());
            };
            domain -= obstruction.shape()->discretizeBoundaryWithDensity(obstruction_fn,
                                                                         obstruction.getType());
        }
    }


    std::pair<vec_t, vec_t> bbox() const{
        return outer_border_->bbox();
    }


    std::list<ObstructionCluster<vec_t>>& getObstructions() {
        return obstruction_list_;
    }
};

#endif //RESTRICTED_CHANNEL_FLOW_OBSTRUCTEDDOMAIN_HPP
