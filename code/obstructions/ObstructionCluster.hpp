#ifndef RESTRICTED_CHANNEL_FLOW_OBSTRUCTIONCLUSTER_HPP
#define RESTRICTED_CHANNEL_FLOW_OBSTRUCTIONCLUSTER_HPP

#include <vector>
#include <utility>
#include <limits>
#include "Obstruction.hpp"

template <class vec_t>
class ObstructionCluster {
private:
    int type_;
    bool overlaps_border_;
    std::vector<std::unique_ptr<Obstruction<vec_t>>> members_;
    std::pair<vec_t, vec_t> bbox_;


    void bboxExpansion(const std::pair<vec_t, vec_t>& other_bbox) {
        bbox_.first = bbox_.first.array().min(other_bbox.first.array());
        bbox_.second = bbox_.second.array().max(other_bbox.second.array());
    }


public:
    explicit ObstructionCluster(Obstruction<vec_t>& founding_member, int type)
            : type_{type}, bbox_{founding_member.bbox()} {
        // vector of non-copyable object doesn't work with initializer list
        members_.push_back(founding_member.asPtr());
    }


    double distance(const vec_t& p) const {
        double min_distance = std::numeric_limits<double>::infinity();
        for (const auto& member_ptr : members_) {
            double distance = member_ptr->distance(p);
            if (distance < min_distance) min_distance = distance;
        }
        return min_distance;
    }


    void join(Obstruction<vec_t>& new_member) {
        for (const auto& member_ptr : members_) {
            if (new_member == *member_ptr) return;
        }
        members_.push_back(new_member.asPtr());
        bboxExpansion(new_member.bbox());
    }


    void join(ObstructionCluster<vec_t>& other_cluster) {
        for (const auto& new_member_ptr : other_cluster.members()) {
            join(*new_member_ptr); // will create a copy to keep both clusters valid
        }
    }


    bool overlap(const Obstruction<vec_t>& other) const {
        for (const auto& member_ptr : members_) {
            if (other.overlap(*member_ptr)) return true;
        }
        return false;
    }


    bool overlap(const ObstructionCluster<vec_t>& other) const {
        for (const auto& member_ptr : members_) {
            if (other.overlap(*member_ptr)) return true;
        }
        return false;
    }


    std::unique_ptr<DomainShape<vec_t>> shape() {
        auto it = members_.begin();
        auto shape = (*it++)->shape();
        while (it != members_.end()) {
            shape = std::make_unique<ShapeUnion<vec_t>>(shape->add(*(*it++)->shape()));
        }
        return shape;
    };


    std::pair<vec_t, vec_t> bbox() const {
        return bbox_;
    }


    int getType() const {
        return type_;
    }

    void setType(int new_type) {
        type_ = new_type;
    }

    int getBorderOverlap() const {
        return overlaps_border_;
    }

    void setBorderOverlap(bool overlaps_border) {
        overlaps_border_ = overlaps_border;
    }

    std::vector<std::unique_ptr<Obstruction<vec_t>>>& members() {
        return members_;
    }
};

#endif //RESTRICTED_CHANNEL_FLOW_OBSTRUCTIONCLUSTER_HPP
