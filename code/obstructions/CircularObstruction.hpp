#ifndef RESTRICTED_CHANNEL_FLOW_CIRCULAROBSTRUCTION_HPP
#define RESTRICTED_CHANNEL_FLOW_CIRCULAROBSTRUCTION_HPP

#include <functional>
#include <random>

#include "Obstruction.hpp"

template <class vec_t>
class CircularObstruction : public Obstruction<vec_t> {
protected:
    double radius_;
    vec_t centre_;


    bool isEqual(const Obstruction<vec_t>& other) const override {
        return centre_.isApprox(static_cast<const CircularObstruction<vec_t>&>(other).centre_)
            && radius_ == static_cast<const CircularObstruction<vec_t>&>(other).radius_;
    }

public:
    explicit CircularObstruction(vec_t& centre, double radius)
                : radius_{radius}, centre_{centre} {}


    explicit CircularObstruction(const std::pair<vec_t, vec_t>& outer_bbox,
                                 std::function<bool (vec_t&)> position_valid,
                                 double min_r, double max_r) {
        // TODO: implement radius based collision check if needed
        double radius = (double)std::rand() / RAND_MAX;
        radius_ = min_r + radius * (max_r - min_r);
        vec_t centre;
        while (true) {
            centre = (vec_t::Random().array() + 1) / 2; // Eigen random returns from [-1, 1] range.
            centre = centre.cwiseProduct(outer_bbox.second - outer_bbox.first)
                     + outer_bbox.first;
            if (position_valid(centre)) break;
        }
        centre_ = centre;
    }


    double distance(const vec_t& p) const override {
        return (centre_ - p).norm() - radius_; // Can be negative when inside.
    }


    bool overlap(const Obstruction<vec_t>& other) const override {
        return other.distance(centre_) <= radius_;
    }


    std::pair<vec_t, vec_t> bbox() const override {
        return std::make_pair(centre_.array() - radius_, centre_.array() + radius_);
    }


    std::unique_ptr<mm::DomainShape<vec_t>> shape() const override {
        return std::make_unique<mm::BallShape<vec_t>>(centre_, radius_);
    }


    std::unique_ptr<Obstruction<vec_t>> asPtr() override{
        return std::make_unique<CircularObstruction<vec_t>>(centre_, radius_);
    }
};

#endif //RESTRICTED_CHANNEL_FLOW_CIRCULAROBSTRUCTION_HPP
