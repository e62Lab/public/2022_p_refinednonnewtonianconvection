#ifndef RESTRICTED_CHANNEL_FLOW_RECTANGULAROBSTRUCTION_HPP
#define RESTRICTED_CHANNEL_FLOW_RECTANGULAROBSTRUCTION_HPP


#include <functional>
#include <random>

#include "Obstruction.hpp"

template <class vec_t>
class RectangularObstruction : public Obstruction<vec_t> {
protected:
    vec_t diag_beg_, diag_end_;
    std::pair<vec_t, vec_t> bbox_;


    bool isEqual(const Obstruction<vec_t>& other) const override {
        return diag_beg_.isApprox(static_cast<const RectangularObstruction<vec_t>&>(other).diag_beg_)
               && diag_end_.isApprox(static_cast<const RectangularObstruction<vec_t>&>(other).diag_end_);
    }

public:
    explicit RectangularObstruction(vec_t& diag_beg, vec_t& diag_end)
            : diag_beg_{diag_beg}, diag_end_{diag_end},
              bbox_{diag_beg.cwiseMin(diag_end), diag_beg.cwiseMax(diag_end)} {}


    explicit RectangularObstruction(const std::pair<vec_t, vec_t>& outer_bbox,
                                 std::function<bool (vec_t&)> position_valid,
                                 double min_size, double max_size) {
        // TODO: implement advanced collision check if needed
        vec_t beg, end;
        while (true) {
            beg = (vec_t::Random().array() + 1) / 2; // Eigen random returns from [-1, 1] range.
            beg = beg.cwiseProduct(outer_bbox.second - outer_bbox.first)
                     + outer_bbox.first;
            vec_t diagonal = (vec_t::Random().array() + 1) / 2;
            diagonal = min_size + diagonal.array() * (max_size - min_size);
            end = beg + diagonal;

            vec_t centre = (beg + end) * 0.5;
            if (position_valid(centre)) break; // TODO: consider changing the validation argument to bbox
        }
        diag_beg_ = beg;
        diag_end_ = end;
        bbox_ = std::make_pair(beg.cwiseMin(end), beg.cwiseMax(end));
    }


    double distance(const vec_t& p) const override {
        auto overlap_count = ((p.array() >= bbox_.first.array())
                              && (p.array() <= bbox_.second.array())).count();
        auto distance = -1 * (p - bbox_.first).cwiseMin((bbox_.second - p));
        if (overlap_count >= p.size() - 1) return distance.maxCoeff();
        return distance.norm();
    }


    bool overlap(const Obstruction<vec_t>& other) const override {
        // TODO: Implement true overlap functionality (check for the closest corner and search along the edge)
        // crude method that only checks the bbox overlap and corner distances and works mainly with other rectangles
        auto bbox_this = bbox();
        auto bbox_other = other.bbox();
        int overlap_count = (   (bbox_other.first.array() >= bbox_this.first.array()
                                 && bbox_other.first.array() <= bbox_this.second.array())
                             || (bbox_other.second.array() >= bbox_this.first.array()
                                 && bbox_other.second.array() <= bbox_this.second.array())).count();
        if (overlap_count >= vec_t::dim) return true;

        for (int subset = 0; subset < (1 << vec_t::dim); ++subset) {
            vec_t corner = vec_t::Zero();
            for (int i = 0; i < vec_t::dim; ++i) {
                if (subset & (1 << i)) {
                    corner[i] = bbox_this.first[i];
                } else {
                    corner[i] = bbox_this.second[i];
                }
            }
            if (other.distance(corner) <= 0) return true;
        }
        return false;
    }


    std::pair<vec_t, vec_t> bbox() const override {
        return bbox_;
    }


    std::unique_ptr<mm::DomainShape<vec_t>> shape() const override {
        return std::make_unique<mm::BoxShape<vec_t>>(diag_beg_, diag_end_);
    }


    std::unique_ptr<Obstruction<vec_t>> asPtr() override {
        return std::make_unique<RectangularObstruction<vec_t>>(diag_beg_, diag_end_);
    }
};


#endif //RESTRICTED_CHANNEL_FLOW_RECTANGULAROBSTRUCTION_HPP
